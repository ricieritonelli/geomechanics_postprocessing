# -*- coding: cp1252 -*-
# ABREVIATIONS USED =============================================================================

# FOR THE CONSTRUCTOR ( TEMPORARIES )

# tao_0 - Normal stress
# permeability - Reference permeability
# phi - Reference porosity
# mi - Viscosity
# K - Bulk modulus
# K_s - Solid bulk modulus
# K_f - Fluid bulk modulus
# G - Sheer modulus
# K_phi - Unjacket pore compressibility
# height - Domain height

# FOR THE CLASS

# timeStepLenght - Lenght of the time step
# TerProNumerical - Terzaghis Problem Numerical
# TerProAnalytical - Terzaghis Problem Analytical

from NumericalGeomechanicsProblem import NumericalGeomechanicsProblem;
from TerzaghisProblemAnalytical import TerzaghisProblemAnalytical;
from TerzaghisProblemNumerical import TerzaghisProblemNumerical;
import matplotlib.pyplot as plt;


# CLASS DEFINITION ==============================================================================

class TerzaghisProblemPlotter( object ):

        # Constructor ---------------------------------------------------------------------------
        def __init__( self, tao_0, permeability, phi, mi, K, K_s, K_f, G, K_phi, numericalDataFileAddress ):
                NumGeoProblem = NumericalGeomechanicsProblem( numericalDataFileAddress );
                height = NumGeoProblem.getHeight();
                self.timeStepLenght = NumGeoProblem.getTimeStepLenght();

                self.TerProNumerical = TerzaghisProblemNumerical( NumGeoProblem );
                self.TerProAnalytical = TerzaghisProblemAnalytical( height, tao_0, permeability, phi, mi, K, K_s, K_f, G, K_phi );


        # Internal functions --------------------------------------------------------------------
        

        # Class interface -----------------------------------------------------------------------
        def plotPressureConstTime( self, *timeValues ):
                positionValuesAnalytical = self.TerProAnalytical.getPositionValues();
                positionValuesNumerical = self.TerProNumerical.getPressurePositionValues();

                plt.figure();

                for i in range( 0, len( timeValues ) ):
                        pressureValuesAnalytical = self.TerProAnalytical.getPressureValuesConstTime( timeValues[ i ] );

                        for j in range( 0, len( pressureValuesAnalytical ) ):
                                pressureValuesAnalytical[ j ] = pressureValuesAnalytical[ j ] / 1000;

                        timeStep = int( timeValues[ i ] / self.timeStepLenght );
                        pressureValuesNumerical = self.TerProNumerical.getPressureValuesConstTime( timeStep );

                        for j in range( 0, len( pressureValuesNumerical ) ):
                                pressureValuesNumerical[ j ] = pressureValuesNumerical[ j ] / 1000;

                        plt.plot( pressureValuesAnalytical, positionValuesAnalytical, label = str( timeValues[ i ] ) );

                        plt.plot( pressureValuesNumerical, positionValuesNumerical, 'ko', markersize = 3 );

                plt.xlabel( 'Pressure [kPa]' );
                plt.ylabel( 'Position [m]' );
                plt.grid( True );
                plt.legend( title = 'Time [s]', fontsize = 12, bbox_to_anchor = ( 1.14, 1.0 ), fancybox = True, shadow = True, numpoints = 1 );

                plt.show();


        def plotPressureNormalizedConstTime( self, *timeValues ):
                positionValuesNormalizedAnalytical = self.TerProAnalytical.getPositionValuesNormalized();
                positionValuesNormalizedNumerical = self.TerProNumerical.getPressurePositionValuesNormalized();

                plt.figure();

                for i in range( 0, len( timeValues ) ):
                        pressureValuesNormalizedAnalytical = self.TerProAnalytical.getPressureValuesNormalizedConstTime( timeValues[ i ] );

                        timeStep = int( timeValues[ i ] / self.timeStepLenght );
                        pressureValuesNormalizedNumerical = self.TerProNumerical.getPressureValuesNormalizedConstTime( timeStep );

                        plt.plot( pressureValuesNormalizedAnalytical, positionValuesNormalizedAnalytical, label = str( timeValues[ i ] ) );

                        plt.plot( pressureValuesNormalizedNumerical, positionValuesNormalizedNumerical, 'ko', markersize = 3 );

                plt.xlabel( 'p/p_0' );
                plt.ylabel( 'y/H' );
                plt.grid( True );
                plt.legend( title = 'Time [s]', fontsize = 12, bbox_to_anchor = ( 1.14, 1.0 ), fancybox = True, shadow = True, numpoints = 1 );

                plt.show();


        def plotDisplacementConstTime( self, *timeValues ):
                positionValuesAnalytical = self.TerProAnalytical.getPositionValues();
                positionValuesNumerical = self.TerProNumerical.getDisplacementPositionValues();

                plt.figure();

                for i in range( 0, len( timeValues ) ):
                        displacementValuesAnalytical = self.TerProAnalytical.getDisplacementValuesConstTime( timeValues[ i ] );

                        for j in range( 0, len( displacementValuesAnalytical ) ):
                                displacementValuesAnalytical[ j ] = displacementValuesAnalytical[ j ] * 1000;

                        timeStep = int( timeValues[ i ] / self.timeStepLenght );
                        displacementValuesNumerical = self.TerProNumerical.getDisplacementValuesConstTime( timeStep );

                        for j in range( 0, len( displacementValuesNumerical ) ):
                                displacementValuesNumerical[ j ] = displacementValuesNumerical[ j ] * 1000;

                        plt.plot( displacementValuesAnalytical, positionValuesAnalytical, label = str( timeValues[ i ] ) );

                        plt.plot( displacementValuesNumerical, positionValuesNumerical, 'ko', markersize = 3 );
                
                plt.xlabel( 'Displacement [mm]' );
                plt.ylabel( 'Position [m]' );
                plt.grid( True );
                plt.legend( title = 'Time [s]', fontsize = 12, loc = 'best', fancybox = True, shadow = True, numpoints = 1 );

                plt.show();


        def plotPressureDomainBase( self, timeIntervalBetweenPoints ):
                timeStepsBetweenPoints = int( timeIntervalBetweenPoints / self.timeStepLenght );

                timeValuesNumerical = self.TerProNumerical.getTimeValues( timeStepsBetweenPoints );
                pressureValuesNumerical = self.TerProNumerical.getPressureValuesConstPositionDomainBase( timeStepsBetweenPoints );

                for j in range( 0, len( pressureValuesNumerical ) ):
                        pressureValuesNumerical[ j ] = pressureValuesNumerical[ j ] / 1000;

                totalTimeInterval = timeValuesNumerical[ len(timeValuesNumerical) - 1 ];
                position = ( self.TerProNumerical.getPressurePositionValues() )[ 0 ];

                timeValuesAnalytical = self.TerProAnalytical.getTimeValues( totalTimeInterval );
                pressureValuesAnalytical = self.TerProAnalytical.getPressureValuesConstPosition( position, totalTimeInterval );

                for j in range( 0, len( pressureValuesAnalytical ) ):
                        pressureValuesAnalytical[ j ] = pressureValuesAnalytical[ j ] / 1000;

                plt.figure();

                plt.plot( timeValuesAnalytical, pressureValuesAnalytical, 'k', label = 'Analytical Solution' );
                plt.plot( timeValuesNumerical, pressureValuesNumerical, 'ro', markersize = 3.5, label = 'Numerical Solution' );
                
                plt.xlabel( 'Time [s]' );
                plt.ylabel( 'Pressure [kPa]' );
                plt.grid( True );
                plt.legend( fontsize = 12, loc = 'best', fancybox = True, shadow = True, numpoints = 1 );

                plt.show();


        def plotPressureNormalizedDomainBase( self, timeIntervalBetweenPoints ):
                timeStepsBetweenPoints = int( timeIntervalBetweenPoints / self.timeStepLenght );

                timeValuesNumerical = self.TerProNumerical.getTimeValues( timeStepsBetweenPoints );
                pressureValuesNormalizedNumerical = self.TerProNumerical.getPressureValuesNormalizedConstPositionDomainBase( timeStepsBetweenPoints );

                totalTimeInterval = timeValuesNumerical[ len(timeValuesNumerical) - 1 ];
                position = ( self.TerProNumerical.getPressurePositionValues() )[ 0 ];

                timeValuesAnalytical = self.TerProAnalytical.getTimeValues( totalTimeInterval );
                pressureValuesNormalizedAnalytical = self.TerProAnalytical.getPressureValuesNormalizedConstPosition( position, totalTimeInterval );

                plt.figure();

                plt.plot( timeValuesAnalytical, pressureValuesNormalizedAnalytical, 'k', label = 'Analytical Solution' );
                plt.plot( timeValuesNumerical, pressureValuesNormalizedNumerical, 'ro', markersize = 3.5, label = 'Numerical Solution' );
                
                plt.xlabel( 'Time [s]' );
                plt.ylabel( 'p/p_0' );
                plt.grid( True );
                plt.legend( fontsize = 12, loc = 'best', fancybox = True, shadow = True, numpoints = 1 );

                plt.show();


        def plotDisplacementDomainTop( self, timeIntervalBetweenPoints ):
                timeStepsBetweenPoints = int( timeIntervalBetweenPoints / self.timeStepLenght );

                timeValuesNumerical = self.TerProNumerical.getTimeValues( timeStepsBetweenPoints );
                displacementValuesNumerical = self.TerProNumerical.getDisplacementValuesConstPositionDomainTop( timeStepsBetweenPoints );

                for i in range( 0, len( displacementValuesNumerical ) ):
                        displacementValuesNumerical[ i ] = displacementValuesNumerical[ i ] * 1000;

                totalTimeInterval = timeValuesNumerical[ len(timeValuesNumerical) - 1 ];
                position = ( self.TerProNumerical.getDisplacementPositionValues() )[ len( self.TerProNumerical.getDisplacementPositionValues() ) - 1 ];
                
                timeValuesAnalytical = self.TerProAnalytical.getTimeValues( totalTimeInterval );
                displacementValuesAnalytical = self.TerProAnalytical.getDisplacementValuesConstPosition( position, totalTimeInterval );

                for i in range( 0, len( displacementValuesAnalytical ) ):
                        displacementValuesAnalytical[ i ] = displacementValuesAnalytical[ i ] * 1000;

                plt.figure();

                plt.plot( timeValuesAnalytical, displacementValuesAnalytical, 'k', label = 'Analytical Solution' );
                plt.plot( timeValuesNumerical, displacementValuesNumerical, 'ro', markersize = 3.5, label = 'Numerical Solution' );
                
                plt.xlabel( 'Time [s]' );
                plt.ylabel( 'Displacement [mm]' );
                plt.grid( True );
                plt.legend( fontsize = 12, loc = 'best', fancybox = True, shadow = True, numpoints = 1 );

                plt.show();
                

# EXAMPLE =======================================================================================


if __name__ == '__main__':
        tao_0 = 1.0e+6;
        permeability = 2.0e-15;
        phi = 0.19;
        mi = 0.001;
        K = 8.0e+9;
        K_s = 3.6e+10;
        K_f = 3.3e+9;
        G = 6.0e+9;
        K_phi = 3.6e+10;
        numericalDataFileAddress = "D:\\PostProcessing\\Plotters\\TerzaghisProblemPlotter\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeomechanicsOneWayCoupling_v1\\Test_01_Data\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeomechanicsSimultaneousSolution_v2\\Test_01_Data\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeoTerzaghisProblemManager\\Test_Results\\SimultaneousPressureConvergence_\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeoTerzaghisProblemManager\\Test_Results_v02\\SimultaneousPressureConvergence_\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeoTerzaghisProblemManager\\Test_Results_v02\\TwoWayPressureConvergence_\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeoTerzaghisProblemManager\\Test_Results_v03\\SimultaneousPressureConvergence_\\PythonTransientResults.dat";

        plotter = TerzaghisProblemPlotter( tao_0, permeability, phi, mi, K, K_s, K_f, G, K_phi, numericalDataFileAddress );
        
