# -*- coding: cp1252 -*-
# ABREVIATIONS USED =============================================================================

# FOR THE CONSTRUCTOR ( TEMPORARIES )

# F - Force applied in the north boundary
# permeability - Reference permeability
# phi - Reference porosity
# mi - Viscosity
# K - Bulk modulus
# K_s - Solid bulk modulus
# K_f - Fluid bulk modulus
# G - Sheer modulus
# K_phi - Unjacket pore compressibility
# height - Domain height
# lenght - Domain lenght

# FOR THE CLASS

# timeStepLenght - Lenght of the time step
# ManProNumerical - Mandel's Problem Numerical
# ManProAnalytical - Mandel's Problem Analytical

from NumericalGeomechanicsProblem import NumericalGeomechanicsProblem;
from MandelsProblemAnalytical import MandelsProblemAnalytical;
from MandelsProblemNumerical import MandelsProblemNumerical;
import matplotlib.pyplot as plt;


# CLASS DEFINITION ==============================================================================

class MandelsProblemPlotter( object ):

        # Constructor ---------------------------------------------------------------------------
        def __init__( self, F, permeability, phi, mi, K, K_s, K_f, G, K_phi, numericalDataFileAddress ):
                NumGeoProblem = NumericalGeomechanicsProblem( numericalDataFileAddress );
                height = NumGeoProblem.getHeight();
                lenght = NumGeoProblem.getLenght();
                self.timeStepLenght = NumGeoProblem.getTimeStepLenght();

                self.ManProNumerical = MandelsProblemNumerical( NumGeoProblem );
                self.ManProAnalytical = MandelsProblemAnalytical( lenght, height, F, permeability, phi, mi, K, K_s, K_f, G, K_phi );


        # Internal functions --------------------------------------------------------------------
        

        # Class interface -----------------------------------------------------------------------
        def plotPressureConstTime( self, *timeValues ):
                positionValuesAnalytical = self.ManProAnalytical.getXPositionValues();
                positionValuesNumerical = self.ManProNumerical.getPressureXPositionValues();

                plt.figure();

                for i in range( 0, len( timeValues ) ):
                        pressureValuesAnalytical = self.ManProAnalytical.getPressureValuesConstTime( timeValues[ i ] );

                        for j in range( 0, len( pressureValuesAnalytical ) ):
                                pressureValuesAnalytical[ j ] = pressureValuesAnalytical[ j ] / 1000;

                        timeStep = int( timeValues[ i ] / self.timeStepLenght );
                        pressureValuesNumerical = self.ManProNumerical.getPressureValuesConstTime( timeStep );

                        for j in range( 0, len( pressureValuesNumerical ) ):
                                pressureValuesNumerical[ j ] = pressureValuesNumerical[ j ] / 1000;

                        plt.plot( positionValuesAnalytical, pressureValuesAnalytical, label = str( timeValues[ i ] ) );

                        plt.plot( positionValuesNumerical, pressureValuesNumerical, 'ko', markersize = 3 );

                plt.xlabel( 'Position [m]' );
                plt.ylabel( 'Pressure [kPa]' );
                plt.grid( True );
                plt.legend( title = 'Time [s]', fontsize = 12, loc = 'best', fancybox = True, shadow = True, numpoints = 1 );

                plt.show();


        def plotPressureNormalizedConstTime( self, *timeValues ):
                positionValuesNormalizedAnalytical = self.ManProAnalytical.getXPositionValuesNormalized();
                positionValuesNormalizedNumerical = self.ManProNumerical.getPressureXPositionValuesNormalized();

                plt.figure();

                for i in range( 0, len( timeValues ) ):
                        pressureValuesNormalizedAnalytical = self.ManProAnalytical.getPressureValuesNormalizedConstTime( timeValues[ i ] );

                        timeStep = int( timeValues[ i ] / self.timeStepLenght );
                        pressureValuesNormalizedNumerical = self.ManProNumerical.getPressureValuesNormalizedConstTime( timeStep );

                        plt.plot( positionValuesNormalizedAnalytical, pressureValuesNormalizedAnalytical, label = str( timeValues[ i ] ) );

                        plt.plot( positionValuesNormalizedNumerical, pressureValuesNormalizedNumerical, 'ko', markersize = 3 );

                plt.xlabel( 'x/L' );
                plt.ylabel( 'p/p_0' );
                plt.grid( True );
                plt.legend( title = 'Time [s]', fontsize = 12, loc = 'best', fancybox = True, shadow = True, numpoints = 1 );

                plt.show();


        def plotVertStressConstTime( self, *timeValues ):
                positionValuesAnalytical = self.ManProAnalytical.getXPositionValues();
                positionValuesNumerical = self.ManProNumerical.getPressureXPositionValues();

                plt.figure();

                for i in range( 0, len( timeValues ) ):
                        vertStressValuesAnalytical = self.ManProAnalytical.getVertStressValuesConstTime( timeValues[ i ] );

                        for j in range( 0, len( vertStressValuesAnalytical ) ):
                                vertStressValuesAnalytical[ j ] = vertStressValuesAnalytical[ j ] / 1000;

                        timeStep = int( timeValues[ i ] / self.timeStepLenght );
                        vertStressValuesNumerical = self.ManProNumerical.getVertStressValuesConstTime( timeStep );

                        for j in range( 0, len( vertStressValuesNumerical ) ):
                                vertStressValuesNumerical[ j ] = vertStressValuesNumerical[ j ] / 1000;

                        plt.plot( positionValuesAnalytical, vertStressValuesAnalytical, label = str( timeValues[ i ] ) );

                        plt.plot( positionValuesNumerical, vertStressValuesNumerical, 'ko', markersize = 3 );

                plt.xlabel( 'Position [m]' );
                plt.ylabel( 'Vertical normal stress [kPa]' );
                plt.grid( True );
                plt.legend( title = 'Time [s]', fontsize = 12, loc = 'best', fancybox = True, shadow = True, numpoints = 1 );

                plt.show();


        def plotHorDisplacementConstTime( self, *timeValues ):
                positionValuesAnalytical = self.ManProAnalytical.getXPositionValues();
                positionValuesNumerical = self.ManProNumerical.getHorDisplacementXPositionValues();

                plt.figure();

                for i in range( 0, len( timeValues ) ):
                        horDisplacementValuesAnalytical = self.ManProAnalytical.getHorDisplacementValuesConstTime( timeValues[ i ] );

                        for j in range( 0, len( horDisplacementValuesAnalytical ) ):
                                horDisplacementValuesAnalytical[ j ] = horDisplacementValuesAnalytical[ j ] * 1000;

                        timeStep = int( timeValues[ i ] / self.timeStepLenght );
                        horDisplacementValuesNumerical = self.ManProNumerical.getHorDisplacementValuesConstTime( timeStep );

                        for j in range( 0, len( horDisplacementValuesNumerical ) ):
                                horDisplacementValuesNumerical[ j ] = horDisplacementValuesNumerical[ j ] * 1000;

                        plt.plot( positionValuesAnalytical, horDisplacementValuesAnalytical, label = str( timeValues[ i ] ) );

                        plt.plot( positionValuesNumerical, horDisplacementValuesNumerical, 'ko', markersize = 3 );
                
                plt.xlabel( 'Position [m]' );
                plt.ylabel( 'Horizontal displacement (u) [mm]' );
                plt.grid( True );
                plt.legend( title = 'Time [s]', fontsize = 12, loc = 'best', fancybox = True, shadow = True, numpoints = 1 );

                plt.show();


        def plotVertDisplacementConstTime( self, *timeValues ):
                positionValuesAnalytical = self.ManProAnalytical.getYPositionValues();
                positionValuesNumerical = self.ManProNumerical.getVertDisplacementYPositionValues();

                plt.figure();

                for i in range( 0, len( timeValues ) ):
                        vertDisplacementValuesAnalytical = self.ManProAnalytical.getVertDisplacementValuesConstTime( timeValues[ i ] );

                        for j in range( 0, len( vertDisplacementValuesAnalytical ) ):
                                vertDisplacementValuesAnalytical[ j ] = vertDisplacementValuesAnalytical[ j ] * 1000;

                        timeStep = int( timeValues[ i ] / self.timeStepLenght );
                        vertDisplacementValuesNumerical = self.ManProNumerical.getVertDisplacementValuesConstTime( timeStep );

                        for j in range( 0, len( vertDisplacementValuesNumerical ) ):
                                vertDisplacementValuesNumerical[ j ] = vertDisplacementValuesNumerical[ j ] * 1000;

                        plt.plot( vertDisplacementValuesAnalytical, positionValuesAnalytical, label = str( timeValues[ i ] ) );

                        plt.plot( vertDisplacementValuesNumerical, positionValuesNumerical, 'ko', markersize = 3 );
                
                plt.xlabel( 'Vertical displacement (v) [mm]' );
                plt.ylabel( 'Position [m]' );
                plt.grid( True );
                plt.legend( title = 'Time [s]', fontsize = 12, loc = 'best', fancybox = True, shadow = True, numpoints = 1 );

                plt.show();


        def plotPressureWestBoundary( self, timeIntervalBetweenPoints ):
                timeStepsBetweenPoints = int( timeIntervalBetweenPoints / self.timeStepLenght );

                timeValuesNumerical = self.ManProNumerical.getTimeValues( timeStepsBetweenPoints );
                pressureValuesNumerical = self.ManProNumerical.getPressureValuesConstPositionSouthWestVolume( timeStepsBetweenPoints );

                for j in range( 0, len( pressureValuesNumerical ) ):
                        pressureValuesNumerical[ j ] = pressureValuesNumerical[ j ] / 1000;

                totalTimeInterval = timeValuesNumerical[ len( timeValuesNumerical ) - 1 ];
                xPosition = ( self.ManProNumerical.getPressureXPositionValues() )[ 0 ];

                timeValuesAnalytical = self.ManProAnalytical.getTimeValues( totalTimeInterval );
                pressureValuesAnalytical = self.ManProAnalytical.getPressureValuesConstPosition( xPosition, totalTimeInterval );

                for j in range( 0, len( pressureValuesAnalytical ) ):
                        pressureValuesAnalytical[ j ] = pressureValuesAnalytical[ j ] / 1000;

                plt.figure();

                plt.plot( timeValuesAnalytical, pressureValuesAnalytical, 'k', label = 'Analytical Solution' );
                plt.plot( timeValuesNumerical, pressureValuesNumerical, 'ro', markersize = 3.5, label = 'Numerical Solution' );
                
                plt.xlabel( 'Time [s]' );
                plt.ylabel( 'Pressure [kPa]' );
                plt.grid( True );
                plt.legend( fontsize = 12, loc = 'best', fancybox = True, shadow = True, numpoints = 1 );

                plt.show();


        def plotPressureNormalizedWestBoundary( self, timeIntervalBetweenPoints ):
                timeStepsBetweenPoints = int( timeIntervalBetweenPoints / self.timeStepLenght );

                timeValuesNumerical = self.ManProNumerical.getTimeValues( timeStepsBetweenPoints );
                pressureValuesNormalizedNumerical = self.ManProNumerical.getPressureValuesNormalizedConstPositionSouthWestVolume( timeStepsBetweenPoints );

                totalTimeInterval = timeValuesNumerical[ len( timeValuesNumerical ) - 1 ];
                xPosition = ( self.ManProNumerical.getPressureXPositionValues() )[ 0 ];

                timeValuesAnalytical = self.ManProAnalytical.getTimeValues( totalTimeInterval );
                pressureValuesNormalizedAnalytical = self.ManProAnalytical.getPressureValuesNormalizedConstPosition( xPosition, totalTimeInterval );

                plt.figure();

                plt.plot( timeValuesAnalytical, pressureValuesNormalizedAnalytical, 'k', label = 'Analytical Solution' );
                plt.plot( timeValuesNumerical, pressureValuesNormalizedNumerical, 'ro', markersize = 3.5, label = 'Numerical Solution' );
                
                plt.xlabel( 'Time [s]' );
                plt.ylabel( 'p/p_0' );
                plt.grid( True );
                plt.legend( fontsize = 12, loc = 'best', fancybox = True, shadow = True, numpoints = 1 );

                plt.show();


        def plotVertStressWestBoundary( self, timeIntervalBetweenPoints ):
                timeStepsBetweenPoints = int( timeIntervalBetweenPoints / self.timeStepLenght );

                timeValuesNumerical = self.ManProNumerical.getTimeValues( timeStepsBetweenPoints );
                vertStressValuesNumerical = self.ManProNumerical.getVertStressValuesConstPositionSouthWestVolume( timeStepsBetweenPoints );

                for j in range( 0, len( vertStressValuesNumerical ) ):
                        vertStressValuesNumerical[ j ] = vertStressValuesNumerical[ j ] / 1000;

                totalTimeInterval = timeValuesNumerical[ len( timeValuesNumerical ) - 1 ];
                xPosition = ( self.ManProNumerical.getPressureXPositionValues() )[ 0 ];

                timeValuesAnalytical = self.ManProAnalytical.getTimeValues( totalTimeInterval );
                vertStressValuesAnalytical = self.ManProAnalytical.getVertStressValuesConstPosition( xPosition, totalTimeInterval );

                for j in range( 0, len( vertStressValuesAnalytical ) ):
                        vertStressValuesAnalytical[ j ] = vertStressValuesAnalytical[ j ] / 1000;

                plt.figure();

                plt.plot( timeValuesAnalytical, vertStressValuesAnalytical, 'k', label = 'Analytical Solution' );
                plt.plot( timeValuesNumerical, vertStressValuesNumerical, 'ro', markersize = 3.5, label = 'Numerical Solution' );
                
                plt.xlabel( 'Time [s]' );
                plt.ylabel( 'Vertical normal stress [kPa]' );
                plt.grid( True );
                plt.legend( fontsize = 12, loc = 'best', fancybox = True, shadow = True, numpoints = 1 );

                plt.show();


        def plotVertStressEastBoundary( self, timeIntervalBetweenPoints ):
                timeStepsBetweenPoints = int( timeIntervalBetweenPoints / self.timeStepLenght );

                timeValuesNumerical = self.ManProNumerical.getTimeValues( timeStepsBetweenPoints );
                vertStressValuesNumerical = self.ManProNumerical.getVertStressValuesConstPositionSouthEastVolume( timeStepsBetweenPoints );

                for j in range( 0, len( vertStressValuesNumerical ) ):
                        vertStressValuesNumerical[ j ] = vertStressValuesNumerical[ j ] / 1000;

                totalTimeInterval = timeValuesNumerical[ len( timeValuesNumerical ) - 1 ];
                xPosition = ( self.ManProNumerical.getPressureXPositionValues() )[ len( self.ManProNumerical.getPressureXPositionValues() ) - 1 ];

                timeValuesAnalytical = self.ManProAnalytical.getTimeValues( totalTimeInterval );
                vertStressValuesAnalytical = self.ManProAnalytical.getVertStressValuesConstPosition( xPosition, totalTimeInterval );

                for j in range( 0, len( vertStressValuesAnalytical ) ):
                        vertStressValuesAnalytical[ j ] = vertStressValuesAnalytical[ j ] / 1000;

                plt.figure();

                plt.plot( timeValuesAnalytical, vertStressValuesAnalytical, 'k', label = 'Analytical Solution' );
                plt.plot( timeValuesNumerical, vertStressValuesNumerical, 'ro', markersize = 3.5, label = 'Numerical Solution' );
                
                plt.xlabel( 'Time [s]' );
                plt.ylabel( 'Vertical normal stress [kPa]' );
                plt.grid( True );
                plt.legend( fontsize = 12, loc = 'best', fancybox = True, shadow = True, numpoints = 1 );

                plt.show();


        def plotHorDisplacementEastBoundary( self, timeIntervalBetweenPoints ):
                timeStepsBetweenPoints = int( timeIntervalBetweenPoints / self.timeStepLenght );

                timeValuesNumerical = self.ManProNumerical.getTimeValues( timeStepsBetweenPoints );
                horDisplacementValuesNumerical = self.ManProNumerical.getHorDisplacementValuesConstPositionEastBoundary( timeStepsBetweenPoints );

                for i in range( 0, len( horDisplacementValuesNumerical ) ):
                        horDisplacementValuesNumerical[ i ] = horDisplacementValuesNumerical[ i ] * 1000;

                totalTimeInterval = timeValuesNumerical[ len( timeValuesNumerical ) - 1 ];
                xPosition = ( self.ManProNumerical.getHorDisplacementXPositionValues() )[ len( self.ManProNumerical.getHorDisplacementXPositionValues() ) - 1 ];

                timeValuesAnalytical = self.ManProAnalytical.getTimeValues( totalTimeInterval );
                horDisplacementValuesAnalytical = self.ManProAnalytical.getHorDisplacementValuesConstPosition( xPosition, totalTimeInterval );

                for i in range( 0, len( horDisplacementValuesAnalytical ) ):
                        horDisplacementValuesAnalytical[ i ] = horDisplacementValuesAnalytical[ i ] * 1000;

                plt.figure();

                plt.plot( timeValuesAnalytical, horDisplacementValuesAnalytical, 'k', label = 'Analytical Solution' );
                plt.plot( timeValuesNumerical, horDisplacementValuesNumerical, 'ro', markersize = 3.5, label = 'Numerical Solution' );
                
                plt.xlabel( 'Time [s]' );
                plt.ylabel( 'Displacement [mm]' );
                plt.grid( True );
                plt.legend( fontsize = 12, loc = 'best', fancybox = True, shadow = True, numpoints = 1 );

                plt.show();


        def plotVertDisplacementNorthBoundary( self, timeIntervalBetweenPoints ):
                timeStepsBetweenPoints = int( timeIntervalBetweenPoints / self.timeStepLenght );

                timeValuesNumerical = self.ManProNumerical.getTimeValues( timeStepsBetweenPoints );
                vertDisplacementValuesNumerical = self.ManProNumerical.getVertDisplacementValuesConstPositionNorthBoundary( timeStepsBetweenPoints );

                for i in range( 0, len( vertDisplacementValuesNumerical ) ):
                        vertDisplacementValuesNumerical[ i ] = vertDisplacementValuesNumerical[ i ] * 1000;

                totalTimeInterval = timeValuesNumerical[ len( timeValuesNumerical ) - 1 ];
                yPosition = ( self.ManProNumerical.getVertDisplacementYPositionValues() )[ len( self.ManProNumerical.getVertDisplacementYPositionValues() ) - 1 ];

                timeValuesAnalytical = self.ManProAnalytical.getTimeValues( totalTimeInterval );
                vertDisplacementValuesAnalytical = self.ManProAnalytical.getVertDisplacementValuesConstPosition( yPosition, totalTimeInterval );

                for i in range( 0, len( vertDisplacementValuesAnalytical ) ):
                        vertDisplacementValuesAnalytical[ i ] = vertDisplacementValuesAnalytical[ i ] * 1000;

                plt.figure();

                plt.plot( timeValuesAnalytical, vertDisplacementValuesAnalytical, 'k', label = 'Analytical Solution' );
                plt.plot( timeValuesNumerical, vertDisplacementValuesNumerical, 'ro', markersize = 3.5, label = 'Numerical Solution' );
                
                plt.xlabel( 'Time [s]' );
                plt.ylabel( 'Displacement [mm]' );
                plt.grid( True );
                plt.legend( fontsize = 12, loc = 'best', fancybox = True, shadow = True, numpoints = 1 );

                plt.show();
                

# EXAMPLE =======================================================================================


if __name__ == '__main__':
        F = 1.0e+8;
        permeability = 2.0e-16;
        phi = 0.19;
        mi = 0.001;
        K = 8.0e+9;
        K_s = 3.6e+10;
        K_f = 3.3e+9;
        G = 6.0e+9;
        K_phi = 3.6e+10;
        numericalDataFileAddress = "D:\\PostProcessing\\Plotters\\MandelsProblemPlotter\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\PostProcessing\\Plotters\\MandelsProblemPlotter\\PythonTransientResults_1.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeomechanicsOneWayCoupling_v3\\Test_01_Data\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeomechanicsOneWayCoupling_v4\\Test_01_Data\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeomechanicsSimultaneousSolution_v4\\Test_01_Data\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeomechanicsSimultaneousSolution_v5\\Test_01_Data\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeoMandelsProblemManager\\Test_Results_v01\\OneWayPreDisBoundCoup_PreConv_\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeoMandelsProblemManager\\Test_Results_v01\\SimultaneousPreDisCoup_BoundConv_PreConv_\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeoMandelsProblemManager\\Test_Results_v01\\SimultaneousPreDisCoup_PreConv_\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeoMandelsProblemManager\\Test_Results_v01\\TwoWayPreDisCoup_PreConv_\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeoMandelsProblemManager\\Test_Results_v01\\OneWayPreDisCoup_PreBoundConv_PreConv_\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeoMandlsProblemManager\\Test_Results_v02\\OneWayPreDisCoup_PreBoundConv_PreConv_\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-eoMandelsProblemManager\\Test_Results_v02\\OneWayPreDisCoup_BoundConv_PreConv_\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeoMandelsProblemManager\\Test_Results_v02\\TwoWayPreDisCoup_PreConv_\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeoMandelsProblemManager\\Test_Results_v05\\TwoWayPreDisCoup_PreConv_IterIt_10\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeoMandelsProblemManager\\Test_Results_v05\\TwoWayPreDisCoup_PreConv_IterIt_3\\PythonTransientResults.dat";

        plotter = MandelsProblemPlotter( F, permeability, phi, mi, K, K_s, K_f, G, K_phi, numericalDataFileAddress );
