# -*- coding: cp1252 -*-
# ABREVIATIONS USED =============================================================================

# FOR THE CONSTRUCTOR ( TEMPORARIES )-----------------

# Properties of the Upper Part

# height_1 - Height
# permeability_1 - Reference permeability
# phi_1 - Reference porosity
# K_1 - Bulk modulus
# K_s_1 - Solid bulk modulus
# G_1 - Sheer modulus
# K_phi_1 - Unjacket pore compressibility

# Properties of the Lower Part

# height_2 - Height
# permeability_2 - Reference permeability
# phi_2 - Reference porosity
# K_2 - Bulk modulus
# K_s_2 - Solid bulk modulus
# G_2 - Sheer modulus
# K_phi_2 - Unjacket pore compressibility

# Properties of the Fluid

# mi - Viscosity
# K_f - Fluid bulk modulus

# p_0_1 - Initial pore pressure in the upper part
# p_0_2 - Initial pore pressure in the lower part

# General Data

# tao_0 - Normal stress

# FOR THE CLASS

# timeStepLenght - Lenght of the time step
# TwoSoilNumerical - Two Layered Soil Numerical
# TwoSoilAnalytical - Two Layered Soil Analytical


from NumericalGeomechanicsProblem import NumericalGeomechanicsProblem;
from TwoLayeredSoilAnalytical import TwoLayeredSoilAnalytical;
from TwoLayeredSoilNumerical import TwoLayeredSoilNumerical;
import matplotlib.pyplot as plt;


# CLASS DEFINITION ==============================================================================

class TwoLayeredSoilPlotter( object ):

        # Constructor ---------------------------------------------------------------------------
        def __init__( self, height_1, permeability_1, phi_1, K_1, K_s_1, G_1, K_phi_1,
                      height_2, permeability_2, phi_2, K_2, K_s_2, G_2, K_phi_2,
                      mi, K_f, tao_0, useNumericalInitialPressureForTheAnalyticalSolution,
                      numericalDataFileAddress ):

                NumGeoProblem = NumericalGeomechanicsProblem( numericalDataFileAddress );
                self.timeStepLenght = NumGeoProblem.getTimeStepLenght();

                self.TwoSoilNumerical = TwoLayeredSoilNumerical( NumGeoProblem );

                p_0_1 = 0;
                p_0_2 = 0;

                if useNumericalInitialPressureForTheAnalyticalSolution == True:
                        p_0_1 = ( NumGeoProblem.getPressureFieldArray[ 0 ] )[ ( NumGeoProblem.getPressureFieldArray[ 0 ] ).shape[ 0 ] - 1, 0 ];
                        p_0_2 = ( NumGeoProblem.getPressureFieldArray[ 0 ] )[ 0, 0 ];

                self.TwoSoilAnalytical = TwoLayeredSoilAnalytical( height_1, permeability_1, phi_1, K_1, K_s_1, G_1, K_phi_1,
                      height_2, permeability_2, phi_2, K_2, K_s_2, G_2, K_phi_2, mi, K_f, tao_0, p_0_1, p_0_2 );


        # Internal functions --------------------------------------------------------------------
        

        # Class interface -----------------------------------------------------------------------
        def plotPressureConstTime( self, *timeValues ):
                positionValuesAnalytical = self.TwoSoilAnalytical.getPositionValues();
                positionValuesNumerical = self.TwoSoilNumerical.getPressurePositionValues();

                plt.figure();

                for i in range( 0, len( timeValues ) ):
                        pressureValuesAnalytical = self.TwoSoilAnalytical.getPressureValuesConstTime( timeValues[ i ] );

                        for j in range( 0, len( pressureValuesAnalytical ) ):
                                pressureValuesAnalytical[ j ] = pressureValuesAnalytical[ j ] / 1000;

                        timeStep = int( timeValues[ i ] / self.timeStepLenght );
                        pressureValuesNumerical = self.TwoSoilNumerical.getPressureValuesConstTime( timeStep );

                        for j in range( 0, len( pressureValuesNumerical ) ):
                                pressureValuesNumerical[ j ] = pressureValuesNumerical[ j ] / 1000;

                        plt.plot( pressureValuesAnalytical, positionValuesAnalytical, label = str( timeValues[ i ] ) );

                        plt.plot( pressureValuesNumerical, positionValuesNumerical, 'ko', markersize = 3 );

                plt.xlabel( 'Pressure [kPa]' );
                plt.ylabel( 'Position [m]' );
                plt.grid( True );
                plt.legend( title = 'Time [s]', fontsize = 12, bbox_to_anchor = ( 1.14, 1.0 ), fancybox = True, shadow = True );

                plt.show();


        def plotPressureNormalizedConstTime( self, *timeValues ):
                positionValuesNormalizedAnalytical = self.TwoSoilAnalytical.getPositionValuesNormalized();
                positionValuesNormalizedNumerical = self.TwoSoilNumerical.getPressurePositionValuesNormalized();

                plt.figure();

                for i in range( 0, len( timeValues ) ):
                        pressureValuesNormalizedAnalytical = self.TwoSoilAnalytical.getPressureValuesNormalizedConstTime( timeValues[ i ] );

                        timeStep = int( timeValues[ i ] / self.timeStepLenght );
                        pressureValuesNormalizedNumerical = self.TwoSoilNumerical.getPressureValuesNormalizedConstTime( timeStep );

                        plt.plot( pressureValuesNormalizedAnalytical, positionValuesNormalizedAnalytical, label = str( timeValues[ i ] ) );

                        plt.plot( pressureValuesNormalizedNumerical, positionValuesNormalizedNumerical, 'ko', markersize = 3 );

                plt.xlabel( 'p/p_0' );
                plt.ylabel( 'y/H' );
                plt.grid( True );
                plt.legend( title = 'Time [s]', fontsize = 12, bbox_to_anchor = ( 1.14, 1.0 ), fancybox = True, shadow = True );

                plt.show();


        def plotDisplacementConstTime( self, *timeValues ):
                positionValuesNumerical = self.TwoSoilNumerical.getDisplacementPositionValues();

                plt.figure();

                for i in range( 0, len( timeValues ) ):

                        timeStep = int( timeValues[ i ] / self.timeStepLenght );
                        displacementValuesNumerical = self.TwoSoilNumerical.getDisplacementValuesConstTime( timeStep );

                        for j in range( 0, len( displacementValuesNumerical ) ):
                                displacementValuesNumerical[ j ] = displacementValuesNumerical[ j ] * 1000;

                        plt.plot( displacementValuesNumerical, positionValuesNumerical, label = str( timeValues[ i ] ) );

                plt.title( 'Numerical Solution' );
                plt.xlabel( 'Displacement [mm]' );
                plt.ylabel( 'Position [m]' );
                plt.grid( True );
                plt.legend( title = 'Time [s]', fontsize = 12, loc = 'best', fancybox = True, shadow = True );

                plt.show();


        def plotPressureDomainBase( self, timeIntervalBetweenPoints ):
                timeStepsBetweenPoints = int( timeIntervalBetweenPoints / self.timeStepLenght );

                timeValuesNumerical = self.TwoSoilNumerical.getTimeValues( timeStepsBetweenPoints );
                pressureValuesNumerical = self.TwoSoilNumerical.getPressureValuesConstPositionDomainBase( timeStepsBetweenPoints );

                for j in range( 0, len( pressureValuesNumerical ) ):
                        pressureValuesNumerical[ j ] = pressureValuesNumerical[ j ] / 1000;

                totalTimeInterval = timeValuesNumerical[ len(timeValuesNumerical) - 1 ];
                position = ( self.TwoSoilNumerical.getPressurePositionValues() )[ 0 ];

                timeValuesAnalytical = self.TwoSoilAnalytical.getTimeValues( totalTimeInterval );
                pressureValuesAnalytical = self.TwoSoilAnalytical.getPressureValuesConstPosition( position, totalTimeInterval );

                for j in range( 0, len( pressureValuesAnalytical ) ):
                        pressureValuesAnalytical[ j ] = pressureValuesAnalytical[ j ] / 1000;

                plt.figure();

                plt.plot( timeValuesAnalytical, pressureValuesAnalytical, 'k', label = 'Analytical Solution' );
                plt.plot( timeValuesNumerical, pressureValuesNumerical, 'ro', markersize = 3.5, label = 'Numerical Solution' );
                
                plt.xlabel( 'Time [s]' );
                plt.ylabel( 'Pressure [kPa]' );
                plt.grid( True );
                plt.legend( fontsize = 12, loc = 'best', fancybox = True, shadow = True, numpoints = 1 );

                plt.show();


        def plotPressureNormalizedDomainBase( self, timeIntervalBetweenPoints ):
                timeStepsBetweenPoints = int( timeIntervalBetweenPoints / self.timeStepLenght );

                timeValuesNumerical = self.TwoSoilNumerical.getTimeValues( timeStepsBetweenPoints );
                pressureValuesNormalizedNumerical = self.TwoSoilNumerical.getPressureValuesNormalizedConstPositionDomainBase( timeStepsBetweenPoints );

                totalTimeInterval = timeValuesNumerical[ len( timeValuesNumerical ) - 1 ];
                position = ( self.TwoSoilNumerical.getPressurePositionValues() )[ 0 ];

                timeValuesAnalytical = self.TwoSoilAnalytical.getTimeValues( totalTimeInterval );
                pressureValuesNormalizedAnalytical = self.TwoSoilAnalytical.getPressureValuesNormalizedConstPosition( position, totalTimeInterval );

                plt.figure();

                plt.plot( timeValuesAnalytical, pressureValuesNormalizedAnalytical, 'k', label = 'Analytical Solution' );
                plt.plot( timeValuesNumerical, pressureValuesNormalizedNumerical, 'ro', markersize = 3.5, label = 'Numerical Solution' );
                
                plt.xlabel( 'Time [s]' );
                plt.ylabel( 'p/p_0' );
                plt.grid( True );
                plt.legend( fontsize = 12, loc = 'best', fancybox = True, shadow = True, numpoints = 1 );

                plt.show();


        def plotDisplacementDomainTop( self, timeIntervalBetweenPoints ):
                timeStepsBetweenPoints = int( timeIntervalBetweenPoints / self.timeStepLenght );

                timeValuesNumerical = self.TwoSoilNumerical.getTimeValues( timeStepsBetweenPoints );
                displacementValuesNumerical = self.TwoSoilNumerical.getDisplacementValuesConstPositionDomainTop( timeStepsBetweenPoints );

                for i in range( 0, len( displacementValuesNumerical ) ):
                        displacementValuesNumerical[ i ] = displacementValuesNumerical[ i ] * 1000;

                plt.figure();

                plt.plot( timeValuesNumerical, displacementValuesNumerical, 'k' );

                plt.title( 'Numerical solution for the displacement "v" on top of the domain', fontsize = 12 );
                plt.xlabel( 'Time [s]' );
                plt.ylabel( 'Displacement [mm]' );
                plt.grid( True );

                plt.show();
                

# EXAMPLE =======================================================================================


if __name__ == '__main__':
        height_1 = 2.5;
        permeability_1 = 2.0e-15;
        phi_1 = 0.19;
        K_1 = 8.0e+9;
        K_s_1 = 3.6e+10;
        G_1 = 6.0e+9;
        K_phi_1 = 3.6e+10;

        height_2 = 7.5;
        permeability_2 = 2.0e-16;
        phi_2 = 0.19;
        K_2 = 8.0e+9;
        K_s_2 = 3.6e+10;
        G_2 = 6.0e+9;
        K_phi_2 = 3.6e+10;

        mi = 0.001;
        K_f = 3.3e+9;
        tao_0 = 1.0e+6;

        useNumericalInitialPressureForTheAnalyticalSolution = False;
        numericalDataFileAddress = "D:\\PostProcessing\\Plotters\\TwoLayeredSoilPlotter\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeomechanicsOneWayCoupling_v2\\Test_01_Data\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeomechanicsSimultaneousSolution_v3\\Test_01_Data\\PythonTransientResults.dat";
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeoTwoLayeredSoilProblemManager\\Test_Results_v1\\SimultaneousPressureConvergence_\\PythonTransientResults.dat"
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeoTwoLayeredSoilProblemManager\\Test_Results_v1\\TwoWayPressureConvergence_\\PythonTransientResults.dat"
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeoTwoLayeredSoilProblemManager\\Test_Results_v1\\OneWayPressureConvergence_\\PythonTransientResults.dat"
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeoTwoLayeredSoilProblemManager\\Test_Results_v2\\SimultaneousPressureConvergence_\\PythonTransientResults.dat"
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeoTwoLayeredSoilProblemManager\\Test_Results_v3\\SimultaneousPressureConvergence_\\PythonTransientResults.dat"
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeoTwoLayeredSoilProblemManager\\Test_Results_v3\\TwoWayPressureConvergence_\\PythonTransientResults.dat"
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeoTwoLayeredSoilProblemManager\\Test_Results_v4\\SimultaneousPressureConvergence_\\PythonTransientResults.dat"
##        numericalDataFileAddress = "D:\\C�digo minicurso\\Reservoir Simulator\\Tests-GeoTwoLayeredSoilProblemManager\\Test_Results_v4\\TwoWayPressureConvergence_\\PythonTransientResults.dat"

        plotter = TwoLayeredSoilPlotter( height_1, permeability_1, phi_1, K_1, K_s_1, G_1, K_phi_1,
                height_2, permeability_2, phi_2, K_2, K_s_2, G_2, K_phi_2,
                mi, K_f, tao_0, useNumericalInitialPressureForTheAnalyticalSolution,
                numericalDataFileAddress );
