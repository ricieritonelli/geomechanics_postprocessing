import math;
import matplotlib.pyplot as plt;

ni = 0.2;
ni_u = 0.33

def getXValues( xMax, numberOfPoints ):
    dx = xMax / ( numberOfPoints - 1 );
    xValues = [ ];

    for i in range( 0, numberOfPoints ):
        xValues.append( i * dx );

    return xValues;


def getYValuesFunction_1( xValues ):
    yValues = [ ];

    for i in xValues:
        yValue = math.tan( i );
        yValues.append( yValue );

    return yValues;


def getYValuesFunction_2( xValues ):
    yValues = [ ];

    for i in xValues:
        yValue = ( ( 1 - ni ) / ( ni_u - ni ) ) * i;
        yValues.append( yValue );

    return yValues;
    

def getYValues( xValues ):
    yValues = [ ];

    for i in xValues:
        yValue = ( ( 1 - ni ) / ( ni_u - ni ) ) * i - math.tan( i );
        yValues.append( yValue );

    return yValues;


def plotFunction( xMax, numberOfPoints ):
    xValues = getXValues( xMax, numberOfPoints );
    yValues = getYValues( xValues );

    plt.figure();
    plt.plot( xValues, yValues, 'r' );
    plt.xlabel( 'x' );
    plt.ylabel( 'y' );
    plt.grid( True );
    plt.show();


def plotBothFunctions( xMax, numberOfPoints ):
    xValues = getXValues( xMax, numberOfPoints );
    yValuesFunction_1 = getYValuesFunction_1( xValues );
    yValuesFunction_2 = getYValuesFunction_2( xValues );

    plt.figure();
    plt.plot( xValues, yValuesFunction_1, 'r' );
    plt.plot( xValues, yValuesFunction_2, 'r' );
    plt.xlabel( 'x' );
    plt.ylabel( 'y' );
    plt.grid( True );
    plt.show();
