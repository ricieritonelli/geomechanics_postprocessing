# ABREVIATIONS USED =============================================================================

# FOR THE CONSTRUCTOR (TEMPORARIES)

# fileName
# fileList - Vector of strings
# fields - Vector of vectors of vectors
# displacementField - Vector of vectors

# FOR THE CLASS

# ny - Number of points in the y direction
# nx - Number of points in the x direction
# height - Domain height
# lenght - Domain lenght
# numberOfFields - Number of fields
# numberOfTimeSteps - Number of time steps with accounted data

# verticesXPositions - Vector
# verticesYPositions - Vector
# nodesXPositions - Vector
# nodesYPositions - Vector
# timeSteps - Vector

# nodesXPositionsArray - Array
# nodesYPositionsArray - Array
# verticesXPositionsArray - Array
# verticesYPositionsArray - Array
# horDisplacementXPositionsArray - Array
# horDisplacementYPositionsArray - Array
# vertDisplacementXPositionsArray - Array
# vertDisplacementYPositionsArray - Array

# pressureField - Vector of vectors
# pressureFieldArray - Vector of arrays
# porosityField - Vector of vectors
# porosityFieldArray - Vector of arrays
# horDisplacementField - Vector of vectors
# horDisplacementFieldArray - Vector or arrays
# vertDisplacementField - Vector of vectors
# vertDisplacementFieldArray - Vector of arrays
# horStressField - Vector of vectors
# horStressFieldArray - Vector of arrays
# vertStressField - Vector of vectors
# vertStressFieldArray - Vector of arrays


import numpy as np;


# CLASS DEFINITION ==============================================================================

class NumericalGeomechanicsProblem( object ):

        # Constructor ---------------------------------------------------------------------------
        def __init__( self, fileAddress ):
                fileName = open( fileAddress );
                fileList = ( fileName ).readlines();
                
                self.nx = int( fileList[ 2 ].split( " " )[ 2 ] );
                self.ny = int( fileList[ 3 ].split( " " )[ 2 ] );
                self.numberOfFields = int( fileList[ 5 ].split( " " )[ 2 ] );

                self._readNumberOfTimeSteps( fileList );
                print "NumberOfTimeSteps read";
                self._readVerticesXPositions( fileList );
                print "VerticesXPositions read";
                self._readVerticesYPositions( fileList );
                print "VerticesYPositions read";
                self._readNodesXPositions( fileList );
                print "NodesXPositions read";
                self._readNodesYPositions( fileList );
                print "NodesYPositions read";
                self._readTimeSteps( fileList );
                print "TimeSteps read";
                
                fields = self._readFields( fileList );

                self._organizeNodesXPositionsArray();
                print "NodesXPositions organized";
                self._organizeNodesYPositionsArray();
                print "NodesYPositions organized";
                self._organizeVerticesXPositionsArray();
                print "VerticesXPositions organized";
                self._organizeVerticesYPositionsArray();
                print "VerticesYPositions organized";
                self._organizeHorDisplacementXPositionsArray();
                print "HorDisplacementXPositions organized";
                self._organizeHorDisplacementYPositionsArray();
                print "HorDisplacementYPositions organized";
                self._organizeVertDisplacementXPositionsArray();
                print "VertDisplacementXPositions organized";
                self._organizeVertDisplacementYPositionsArray();
                print "VertDisplacementYPositions organized";

                self._organizePressureField( fields );
                print "PressureField organized";
                self._organizePorosityField( fields );
                print "PorosityField organized";
                self._organizeDisplacementField( fields );
                print "DisplacementField organized";
                if self.numberOfFields > 3:
                        self._organizeHorStressField( fields );
                        print "HorStressField organized";
                if self.numberOfFields > 4:
                        self._organizeVertStressField( fields );
                        print "VertStressField organized";

                self._calculateHeight();
                self._calculateLenght();
                self._calculateTimeStepLenght();


        # Internal functions --------------------------------------------------------------------
        def _readNumberOfTimeSteps( self, fileList ):
                numberOfLines = len( fileList ) - 21;
                self.numberOfTimeSteps = numberOfLines / ( 3 * self.numberOfFields );


        def _readVerticesXPositions( self, fileList ):
                i = 0;
                self.verticesXPositions = [ ];
                while fileList[ 10 ].split( " " )[ i ] != "\n":
                        self.verticesXPositions.append( float( fileList[ 10 ].split( " " )[ i ] ) );
                        i += 1;


        def _readVerticesYPositions( self, fileList ):
                i = 0;
                self.verticesYPositions = [ ];
                while fileList[ 13 ].split( " " )[ i ] != "\n":
                        self.verticesYPositions.append( float( fileList[ 13 ].split( " " )[ i ] ) );
                        i += 1;


        def _readNodesXPositions( self, fileList ):
                i = 0;
                self.nodesXPositions = [ ];
                while fileList[ 16 ].split( " " )[ i ] != "\n":
                        self.nodesXPositions.append( float( fileList[ 16 ].split( " " )[ i ] ) );
                        i += 1;


        def _readNodesYPositions( self, fileList ):
                i = 0;
                self.nodesYPositions = [ ];
                while fileList[ 19 ].split( " " )[ i ] != "\n":
                        self.nodesYPositions.append( float( fileList[ 19 ].split( " " )[ i ] ) );
                        i += 1;


        def _readTimeSteps( self, fileList ):
                self.timeSteps = [ ];
                for i in range( 0, self.numberOfTimeSteps ):
                        self.timeSteps.append( float( fileList[ 21 + i * self.numberOfFields * 3 ].split( " " )[ 3 ] ) );


        def _readFields( self, fileList ):
                fields = [ ];
                
                for i in range( 0, self.numberOfFields ):
                        fields.append( [ ] );
                        
                        for j in range( 0, self.numberOfTimeSteps ):
                                fields[ i ].append( [ ] );

                                k = 0;
                                while fileList[ ( 22 + ( i * 3 ) ) + ( j * self.numberOfFields * 3 ) ].split( " " )[ k ] != "\n":
                                        fields[ i ][ j ].append( float( fileList[ ( 22 + ( i * 3 ) ) + ( j * self.numberOfFields * 3 ) ].split( " " )[ k ] ) );
                                        k += 1;

                        print "Field " + str( i ) + " read";

                return fields;


        def _organizeNodesXPositionsArray( self ):
                self.nodesXPositionsArray = np.zeros( ( self.ny, self.nx ) );
                for i in range( 0, self.ny ):
                        for j in range( 0, self.nx ):
                                self.nodesXPositionsArray[ i, j ] = self.nodesXPositions[ ( i * self.nx ) + j ];


        def _organizeNodesYPositionsArray( self ):
                self.nodesYPositionsArray = np.zeros( ( self.ny, self.nx ) );
                for i in range( 0, self.ny ):
                        for j in range( 0, self.nx ):
                                self.nodesYPositionsArray[ i, j ] = self.nodesYPositions[ ( i * self.nx ) + j ];


        def _organizeVerticesXPositionsArray( self ):
                self.verticesXPositionsArray = np.zeros( ( self.ny + 1, self.nx + 1 ) );
                for i in range( 0, self.ny + 1 ):
                        for j in range( 0, self.nx + 1 ):
                                self.verticesXPositionsArray[ i, j ] = self.verticesXPositions[ ( i * ( self.nx + 1 ) ) + j ];


        def _organizeVerticesYPositionsArray( self ):
                self.verticesYPositionsArray = np.zeros( ( self.ny + 1, self.nx + 1 ) );
                for i in range( 0, self.ny + 1 ):
                        for j in range( 0, self.nx + 1 ):
                                self.verticesYPositionsArray[ i, j ] = self.verticesYPositions[ ( i * ( self.nx + 1 ) ) + j ];


        def _organizeHorDisplacementXPositionsArray( self ):
                self.horDisplacementXPositionsArray = np.zeros( ( self.ny, self.nx + 1 ) );
                for i in range( 0, self.ny ):
                        for j in range( 0, self.nx + 1 ):
                                self.horDisplacementXPositionsArray[ i, j ] = self.verticesXPositionsArray[ 1, j ];


        def _organizeHorDisplacementYPositionsArray( self ):
                self.horDisplacementYPositionsArray = np.zeros( ( self.ny, self.nx + 1 ) );
                for i in range( 0, self.ny ):
                        for j in range( 0, self.nx + 1 ):
                                self.horDisplacementYPositionsArray[ i, j ] = self.nodesYPositionsArray[ i, 1 ];


        def _organizeVertDisplacementXPositionsArray( self ):
                self.vertDisplacementXPositionsArray = np.zeros( ( self.ny + 1, self.nx ) );
                for i in range( 0, self.ny + 1 ):
                        for j in range( 0, self.nx ):
                                self.vertDisplacementXPositionsArray[ i, j ] = self.nodesXPositionsArray[ 1, j ];


        def _organizeVertDisplacementYPositionsArray( self ):
                self.vertDisplacementYPositionsArray = np.zeros( ( self.ny + 1, self.nx ) );
                for i in range( 0, self.ny + 1 ):
                        for j in range( 0, self.nx ):
                                self.vertDisplacementYPositionsArray[ i, j ] = self.verticesYPositionsArray[ i, 1 ];


        def _organizePressureField( self, fields ):
                self.pressureField = fields[ 0 ];
                self.pressureFieldArray = [ ];
                
                for k in range( 0, len( self.pressureField ) ):
                        self.pressureFieldArray.append( np.zeros( ( self.ny, self.nx ) ) );

                        for i in range( 0, self.ny ):
                                for j in range( 0, self.nx ):
                                        self.pressureFieldArray[ k ][ i, j ] = self.pressureField[ k ][ ( i * self.nx ) + j ];


        def _organizePorosityField( self, fields ):
                self.porosityField = fields[ 1 ];
                self.porosityFieldArray = [ ];

                for k in range( 0, len( self.porosityField ) ):
                        self.porosityFieldArray.append( np.zeros( ( self.ny, self.nx ) ) );

                        for i in range( 0, self.ny ):
                                for j in range( 0, self.nx ):
                                        self.porosityFieldArray[ k ][ i, j ] = self.porosityField[ k ][ ( i * self.nx ) + j ];


        def _organizeDisplacementField( self, fields ):
                displacementField = fields[ 2 ];
                self._organizeHorDisplacementField( displacementField );
                self._organizeVertDisplacementField( displacementField );


        def _organizeHorDisplacementField( self, displacementField ):
                self.horDisplacementField = [ ];
                
                for k in range( 0, len( displacementField ) ):
                        self.horDisplacementField.append( np.zeros( ( ( self.nx + 1 ) * self.ny, 1 ) ) );

                        for i in range( 0, self.ny ):
                                self.horDisplacementField[ k ][ i * ( self.nx + 1 ) ] = \
                                                           displacementField[ k ][ ( ( self.nx - 1 ) * self.ny ) + ( ( self.ny - 1 ) * self.nx ) + i ];
                                self.horDisplacementField[ k ][ ( i * ( self.nx + 1 ) ) + self.nx ] = \
                                                           displacementField[ k ][ ( ( self.nx - 1 ) * self.ny ) + ( ( self.ny - 1 ) * self.nx ) + self.ny + i ];

                        for i in range( 0, self.ny ):
                                for j in range( 0, self.nx - 1 ):
                                        self.horDisplacementField[ k ][ ( i * ( self.nx + 1 ) ) + j + 1 ] = displacementField[ k ][ i * ( self.nx - 1 ) + j ];

                self.horDisplacementFieldArray = [ ];

                for k in range( 0, len( self.horDisplacementField ) ):
                        self.horDisplacementFieldArray.append( np.zeros( ( self.ny, self.nx + 1 ) ) );

                        for i in range( 0, self.ny ):
                                for j in range( 0, self.nx + 1 ):
                                        self.horDisplacementFieldArray[ k ][ i, j ] = self.horDisplacementField[ k ][ ( i * ( self.nx + 1 ) ) + j ];


        def _organizeVertDisplacementField( self, displacementField ):
                self.vertDisplacementField = [ ];

                for k in range( 0, len( displacementField ) ):
                        self.vertDisplacementField.append( np.zeros( ( ( self.ny + 1 ) * self.nx, 1 ) ) );

                        for i in range( 0, self.nx ):
                                self.vertDisplacementField[ k ][ i ] = \
                                                            displacementField[ k ][ ( ( self.nx - 1 ) * self.ny) + ( self.nx * ( self.ny - 1 ) ) + 2 * self.ny + i ];
                                self.vertDisplacementField[ k ][ ( self.nx * self.ny ) + i ] = \
                                                            displacementField[ k ][ ( ( self.nx - 1 ) * self.ny) + ( self.nx * ( self.ny - 1 ) ) + 2 * self.ny + self.nx + i ];

                        for i in range( 0, self.nx * ( self.ny - 1 ) ):
                                self.vertDisplacementField[ k ][ self.nx + i ] = displacementField[ k ][ ( ( self.nx - 1 ) * self.ny ) + i ];

                self.vertDisplacementFieldArray = [ ];

                for k in range( 0, len( self.vertDisplacementField ) ):
                        self.vertDisplacementFieldArray.append( np.zeros( ( self.ny + 1, self.nx ) ) );

                        for i in range( 0, self.ny + 1 ):
                                for j in range( 0, self.nx ):
                                        self.vertDisplacementFieldArray[ k ][ i, j ] = self.vertDisplacementField[ k ][ ( i * self.nx ) + j ];


        def _organizeHorStressField( self, fields ):
                self.horStressField = fields[ 3 ];
                self.horStressFieldArray = [ ];

                for k in range( 0, len( self.horStressField ) ):
                        self.horStressFieldArray.append( np.zeros( ( self.ny, self.nx ) ) );

                        for i in range( 0, self.ny ):
                                for j in range( 0, self.nx ):
                                        self.horStressFieldArray[ k ][ i, j ] = self.horStressField[ k ][ ( i * self.nx ) + j ];


        def _organizeVertStressField( self, fields ):
                self.vertStressField = fields[ 4 ];
                self.vertStressFieldArray = [ ];

                for k in range( 0, len( self.vertStressField ) ):
                        self.vertStressFieldArray.append( np.zeros( ( self.ny, self.nx ) ) );

                        for i in range( 0, self.ny ):
                                for j in range( 0, self.nx ):
                                        self.vertStressFieldArray[ k ][ i, j ] = self.vertStressField[ k ][ ( i * self.nx ) + j ];


        def _calculateHeight( self ):
                self.height = abs( self.verticesYPositionsArray[ 0, 0 ] - self.verticesYPositionsArray[ self.ny, 0 ] );


        def _calculateLenght( self ):
                self.lenght = abs( self.verticesXPositionsArray[ 0, 0 ] - self.verticesXPositionsArray[ 0, self.nx ] );


        def _calculateTimeStepLenght( self ):
                if len( self.timeSteps ) > 1:
                        self.timeStepLenght = self.timeSteps[ 1 ] - self.timeSteps[ 0 ];
                else:
                        self.timeStepLenght = 0.0;


        # Class interface -----------------------------------------------------------------------
        def getNy( self ):
                return self.ny;

        def getNx( self ):
                return self.nx;

        def getNumberOfTimeSteps( self ):
                return self.numberOfTimeSteps;

        def getTimeSteps( self ):
                return self.timeSteps;

        def getNodesXPositionsArray( self ):
                return self.nodesXPositionsArray;

        def getNodesYPositionsArray( self ):
                return self.nodesYPositionsArray;

        def getVerticesXPositionsArray( self ):
                return self.verticesXPositionsArray;

        def getVerticesYPositionsArray( self ):
                return self.verticesYPositionsArray;

        def getHorDisplacementXPositionsArray( self ):
                return self.horDisplacementXPositionsArray;

        def getHorDisplacementYPositionsArray( self ):
                return self.horDisplacementYPositionsArray;

        def getVertDisplacementXPositionsArray( self ):
                return self.vertDiplacementXPositionsArray;

        def getVertDisplacementYPositionsArray( self ):
                return self.vertDisplacementYPositionsArray;

        def getPressureFieldArray( self ):
                return self.pressureFieldArray;

        def getPorosityFieldArray( self ):
                return self.porosityFieldArray;

        def getHorDisplacementFieldArray( self ):
                return self.horDisplacementFieldArray;

        def getVertDisplacementFieldArray( self ):
                return self.vertDisplacementFieldArray;

        def getHorStressFieldArray( self ):
                try:
                        return self.horStressFieldArray;
                except:
                        print "The file read does not containg horStressField data.";

        def getVertStressFieldArray( self ):
                try:
                        return self.vertStressFieldArray;
                except:
                        print "The file read does not containg vertStressField data.";

        def getHeight( self ):
                return self.height;

        def getLenght( self ):
                return self.lenght;

        def getTimeStepLenght( self ):
                return self.timeStepLenght;
        
# EXAMPLE =======================================================================================

import matplotlib.pyplot as plt

if __name__ == '__main__':
        example = NumericalGeomechanicsProblem( "D:\\PostProcessing\\Numerical\\Python\\00_Example.dat" );
