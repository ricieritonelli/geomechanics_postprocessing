# ABREVIATIONS USED =============================================================================


from NumericalGeomechanicsProblem import NumericalGeomechanicsProblem;


# CLASS DEFINITION ==============================================================================

class TerzaghisProblemNumerical( object ):

        # Constructor ---------------------------------------------------------------------------
        def __init__( self, NumericalGeomechanicsProblem ):
                self.numericalSolution = NumericalGeomechanicsProblem;
                

        # Internal functions --------------------------------------------------------------------


        # Class interface -----------------------------------------------------------------------
        def getPressurePositionValues( self ):
                positionValues = [ ];
                
                for i in range( 0, ( ( self.numericalSolution.getNodesYPositionsArray() ).shape )[ 0 ] ):
                        positionValues.append( ( self.numericalSolution.getNodesYPositionsArray() )[ i, 0 ] );
                        
                return positionValues;

        
        def getPressurePositionValuesNormalized( self ):
                positionValues = self.getPressurePositionValues();
                positionValuesNormalized = [ ];
                height = self.numericalSolution.getHeight();

                for i in range( 0, len( positionValues ) ):
                        positionValuesNormalized.append( positionValues[ i ] / height );

                return positionValuesNormalized;


        def getDisplacementPositionValues( self ):
                positionValues = [ ];

                for i in range( 0, ( ( self.numericalSolution.getVertDisplacementYPositionsArray() ).shape )[ 0 ] ):
                        positionValues.append( ( self.numericalSolution.getVertDisplacementYPositionsArray() )[ i, 0 ] );

                return positionValues;


        def getDisplacementPositionValuesNormalized( self ):
                positionValues = self.getDisplacementPositionValues();
                positionValuesNormalized = [ ];
                height = self.numericalSolution.getHeight();

                for i in range( 0, len( positionValues ) ):
                        positionValuesNormalized.append( positionValues[ i ] / height );

                return positionValuesNormalized;


        def getPressureValuesConstTime( self, timeStep ):
                pressureValues = [ ];

                for i in range( 0, ( ( self.numericalSolution.getPressureFieldArray()[ timeStep ] ).shape )[ 0 ] ):
                        pressureValues.append( ( self.numericalSolution.getPressureFieldArray()[ timeStep ] )[ i, 0 ] );

                return pressureValues;


        def getPressureValuesNormalizedConstTime( self, timeStep ):
                pressureValues = self.getPressureValuesConstTime( timeStep );
                pressureValuesNormalized = [ ];
                initialPressure = ( self.numericalSolution.getPressureFieldArray()[ 0 ] )[ 0, 0 ];

                for i in range( 0, len( pressureValues ) ):
                        pressureValuesNormalized.append( pressureValues[ i ] / initialPressure );

                return pressureValuesNormalized;


        def getDisplacementValuesConstTime( self, timeStep ):
                displacementValues = [ ];

                for i in range( 0, ( ( self.numericalSolution.getVertDisplacementFieldArray()[ timeStep ] ).shape )[ 0 ] ):
                        displacementValues.append( ( self.numericalSolution.getVertDisplacementFieldArray()[ timeStep ] )[ i, 0 ] );

                return displacementValues;


        def getTimeValues( self, timeStepsBetweenPoints ):
                timeValues = [ ];

                i = 0;
                while i < len( self.numericalSolution.getTimeSteps() ):
                        timeValues.append( ( self.numericalSolution.getTimeSteps() )[ i ] );
                        i += timeStepsBetweenPoints;

                return timeValues;


        def getPressureValuesConstPositionDomainBase( self, timeStepsBetweenPoints ):
                pressureValues = [ ];

                i = 0;
                while i < len( self.numericalSolution.getPressureFieldArray() ):
                        pressureValues.append( ( self.numericalSolution.getPressureFieldArray()[ i ] )[ 0, 0 ] );
                        i += timeStepsBetweenPoints;

                return pressureValues;


        def getPressureValuesNormalizedConstPositionDomainBase( self, timeStepsBetweenPoints ):
                pressureValues = self.getPressureValuesConstPositionDomainBase( timeStepsBetweenPoints );
                pressureValuesNormalized = [ ];
                initialPressure = ( self.numericalSolution.getPressureFieldArray()[ 0 ] )[ 0, 0 ];

                for i in range( 0, len( pressureValues ) ):
                        pressureValuesNormalized.append( pressureValues[ i ] / initialPressure );

                return pressureValuesNormalized;


        def getDisplacementValuesConstPositionDomainTop( self, timeStepsBetweenPoints ):
                displacementValues = [ ];

                i = 0;
                while i < len( self.numericalSolution.getVertDisplacementFieldArray() ):
                        displacementValues.append( ( self.numericalSolution.getVertDisplacementFieldArray()[ i ] )
                                                   [ ( self.numericalSolution.getVertDisplacementFieldArray()[ i ] ).shape[ 0 ] - 1, 0 ] );
                        i += timeStepsBetweenPoints;

                return displacementValues;

        
# EXAMPLE =======================================================================================

import matplotlib.pyplot as plt

if __name__ == '__main__':
        results = NumericalGeomechanicsProblem( "D:\\PostProcessing\\Numerical\\Python\\TerzaghisProblemNumerical\\00_Example.dat" );
        example = TerzaghisProblemNumerical( results );
        timeValues = example.getTimeValues( 2 );


##        plt.figure();
##
##        PressurePositionValues = example.getPressurePositionValues();
##
##        PressureValuesConstTime01 = example.getPressureValuesConstTime( 1 );        
##        PressureValuesConstTime10 = example.getPressureValuesConstTime( 10 );        
##        PressureValuesConstTime25 = example.getPressureValuesConstTime( 25 );        
##        PressureValuesConstTime50 = example.getPressureValuesConstTime( 50 );        
##        PressureValuesConstTime100 = example.getPressureValuesConstTime( 100 );
##
##        plt.plot( PressureValuesConstTime01, PressurePositionValues, 'bo', markersize = 4, label = '1' );
##        plt.plot( PressureValuesConstTime10, PressurePositionValues, 'go', markersize = 4, label = '10' );
##        plt.plot( PressureValuesConstTime25, PressurePositionValues, 'ro', markersize = 4, label = '25' );
##        plt.plot( PressureValuesConstTime50, PressurePositionValues, 'co', markersize = 4, label = '50' );
##        plt.plot( PressureValuesConstTime100, PressurePositionValues, 'mo', markersize = 4, label = '100' );
##
##        plt.xlabel( 'Pressure [Pa]' );
##        plt.ylabel( 'Position [m]' );
##        plt.legend( title = 'Time [s]', fontsize = 12, bbox_to_anchor = ( 1.14, 1.0 ) ); # bbox_to_anchor is used to push the legend around


##        plt.figure();
##
##        PressurePositionValuesNormalized = example.getPressurePositionValuesNormalized();
##
##        PressureValuesNormalizedConstTime01 = example.getPressureValuesNormalizedConstTime( 1 );        
##        PressureValuesNormalizedConstTime10 = example.getPressureValuesNormalizedConstTime( 10 );        
##        PressureValuesNormalizedConstTime25 = example.getPressureValuesNormalizedConstTime( 25 );        
##        PressureValuesNormalizedConstTime50 = example.getPressureValuesNormalizedConstTime( 50 );        
##        PressureValuesNormalizedConstTime100 = example.getPressureValuesNormalizedConstTime( 100 );
##
##        plt.plot( PressureValuesNormalizedConstTime01, PressurePositionValuesNormalized, 'bo', markersize = 4, label = '1' );
##        plt.plot( PressureValuesNormalizedConstTime10, PressurePositionValuesNormalized, 'go', markersize = 4, label = '10' );
##        plt.plot( PressureValuesNormalizedConstTime25, PressurePositionValuesNormalized, 'ro', markersize = 4, label = '25' );
##        plt.plot( PressureValuesNormalizedConstTime50, PressurePositionValuesNormalized, 'co', markersize = 4, label = '50' );
##        plt.plot( PressureValuesNormalizedConstTime100, PressurePositionValuesNormalized, 'mo', markersize = 4, label = '100' );
##
##        plt.xlabel( 'p / p_0' );
##        plt.ylabel( 'y / H' );
##        plt.legend( title = 'Time [s]', fontsize = 12, bbox_to_anchor = ( 1.14, 1.0 ) ); # bbox_to_anchor is used to push the legend around


##        plt.figure();
##
##        DisplacementPositionValues = example.getDisplacementPositionValues();
##
##        DisplacementValuesConstTime00 = example.getDisplacementValuesConstTime( 0 );
##        DisplacementValuesConstTime25 = example.getDisplacementValuesConstTime( 25 );
##        DisplacementValuesConstTime50 = example.getDisplacementValuesConstTime( 50 );
##        DisplacementValuesConstTime100 = example.getDisplacementValuesConstTime( 100 );
##
##        plt.plot( DisplacementValuesConstTime00, DisplacementPositionValues, 'bo', markersize = 4, label = '0' );
##        plt.plot( DisplacementValuesConstTime25, DisplacementPositionValues, 'go', markersize = 4, label = '25' );
##        plt.plot( DisplacementValuesConstTime50, DisplacementPositionValues, 'ro', markersize = 4, label = '50' );
##        plt.plot( DisplacementValuesConstTime100, DisplacementPositionValues, 'co', markersize = 4, label = '100' );
##
##        plt.xlabel( 'Displacement [m]' );
##        plt.ylabel( 'Position [m]' );
##        plt.legend( title = 'Time [s]', fontsize = 12 );


##        plt.figure();
##
##        PressureValuesConstPosition = example.getPressureValuesConstPositionDomainBase( 2 );
##
##        plt.plot( timeValues, PressureValuesConstPosition, 'ro', markersize = 3, label = 'Numerical Solution' );
##
##        plt.xlabel( 'Time [s]' );
##        plt.ylabel( 'Pressure [Pa]' );
##        plt.legend( fontsize = 12 );


##        plt.figure();
##
##        PressureValuesNormalizedConstPosition = example.getPressureValuesNormalizedConstPositionDomainBase( 2 );
##
##        plt.plot( timeValues, PressureValuesNormalizedConstPosition, 'ro', markersize = 3, label = 'Numerical Solution' );
##
##        plt.xlabel( 'Time [s]' );
##        plt.ylabel( 'p/p_0' );
##        plt.legend( fontsize = 12 );


##        plt.figure();
##
##        DisplacementValuesConstPosition = example.getDisplacementValuesConstPositionDomainTop( 2 );
##
##        plt.plot( timeValues, DisplacementValuesConstPosition, 'ro', markersize = 3, label = 'Numerical Solution' );
##
##        plt.xlabel( 'Time [s]' );
##        plt.ylabel( 'Displacement [m]' );
##        plt.legend( fontsize = 12 );
        

        plt.show();
