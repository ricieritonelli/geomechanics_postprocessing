# ABREVIATIONS USED =============================================================================


from NumericalGeomechanicsProblem import NumericalGeomechanicsProblem;


# CLASS DEFINITION ==============================================================================

class TwoLayeredSoilNumerical( object ):

        # Constructor ---------------------------------------------------------------------------
        def __init__( self, NumericalGeomechanicsProblem ):
                self.numericalSolution = NumericalGeomechanicsProblem;
                

        # Internal functions --------------------------------------------------------------------


        # Class interface -----------------------------------------------------------------------
        def getPressurePositionValues( self ):
                positionValues = [ ];
                
                for i in range( 0, ( ( self.numericalSolution.getNodesYPositionsArray() ).shape )[ 0 ] ):
                        positionValues.append( ( self.numericalSolution.getNodesYPositionsArray() )[ i, 0 ] );
                        
                return positionValues;

        
        def getPressurePositionValuesNormalized( self ):
                positionValues = self.getPressurePositionValues();
                positionValuesNormalized = [ ];
                height = self.numericalSolution.getHeight();

                for i in range( 0, len( positionValues ) ):
                        positionValuesNormalized.append( positionValues[ i ] / height );

                return positionValuesNormalized;


        def getDisplacementPositionValues( self ):
                positionValues = [ ];

                for i in range( 0, ( ( self.numericalSolution.getVertDisplacementYPositionsArray() ).shape )[ 0 ] ):
                        positionValues.append( ( self.numericalSolution.getVertDisplacementYPositionsArray() )[ i, 0 ] );

                return positionValues;


        def getDisplacementPositionValuesNormalized( self ):
                positionValues = self.getDisplacementPositionValues();
                positionValuesNormalized = [ ];
                height = self.numericalSolution.getHeight();

                for i in range( 0, len( positionValues ) ):
                        positionValuesNormalized.append( positionValues[ i ] / height );

                return positionValuesNormalized;


        def getPressureValuesConstTime( self, timeStep ):
                pressureValues = [ ];

                for i in range( 0, ( ( self.numericalSolution.getPressureFieldArray()[ timeStep ] ).shape )[ 0 ] ):
                        pressureValues.append( ( self.numericalSolution.getPressureFieldArray()[ timeStep ] )[ i, 0 ] );

                return pressureValues;


        def getPressureValuesNormalizedConstTime( self, timeStep ):
                pressureValues = self.getPressureValuesConstTime( timeStep );
                pressureValuesNormalized = [ ];
                initialPressure = ( self.numericalSolution.getPressureFieldArray()[ 0 ] )[ 0, 0 ];

                for i in range( 0, len( pressureValues ) ):
                        pressureValuesNormalized.append( pressureValues[ i ] / initialPressure );

                return pressureValuesNormalized;


        def getDisplacementValuesConstTime( self, timeStep ):
                displacementValues = [ ];

                for i in range( 0, ( ( self.numericalSolution.getVertDisplacementFieldArray()[ timeStep ] ).shape )[ 0 ] ):
                        displacementValues.append( ( self.numericalSolution.getVertDisplacementFieldArray()[ timeStep ] )[ i, 0 ] );

                return displacementValues;


        def getTimeValues( self, timeStepsBetweenPoints ):
                timeValues = [ ];

                i = 0;
                while i < len( self.numericalSolution.getTimeSteps() ):
                        timeValues.append( ( self.numericalSolution.getTimeSteps() )[ i ] );
                        i += timeStepsBetweenPoints;

                return timeValues;


        def getPressureValuesConstPositionDomainBase( self, timeStepsBetweenPoints ):
                pressureValues = [ ];

                i = 0;
                while i < len( self.numericalSolution.getPressureFieldArray() ):
                        pressureValues.append( ( self.numericalSolution.getPressureFieldArray()[ i ] )[ 0, 0 ] );
                        i += timeStepsBetweenPoints;

                return pressureValues;


        def getPressureValuesNormalizedConstPositionDomainBase( self, timeStepsBetweenPoints ):
                pressureValues = self.getPressureValuesConstPositionDomainBase( timeStepsBetweenPoints );
                pressureValuesNormalized = [ ];
                initialPressure = ( self.numericalSolution.getPressureFieldArray()[ 0 ] )[ 0, 0 ];

                for i in range( 0, len( pressureValues ) ):
                        pressureValuesNormalized.append( pressureValues[ i ] / initialPressure );

                return pressureValuesNormalized;


        def getDisplacementValuesConstPositionDomainTop( self, timeStepsBetweenPoints ):
                displacementValues = [ ];

                i = 0;
                while i < len( self.numericalSolution.getVertDisplacementFieldArray() ):
                        displacementValues.append( ( self.numericalSolution.getVertDisplacementFieldArray()[ i ] )
                                                   [ ( self.numericalSolution.getVertDisplacementFieldArray()[ i ] ).shape[ 0 ] - 1, 0 ] );
                        i += timeStepsBetweenPoints;

                return displacementValues;

        
# EXAMPLE =======================================================================================

import matplotlib.pyplot as plt

if __name__ == '__main__':
        results = NumericalGeomechanicsProblem( "D:\\PostProcessing\\Numerical\\Python\\TwoLayeredSoilNumerical\\PythonTransientResults.dat" );
        example = TwoLayeredSoilNumerical( results );
        timeValues = example.getTimeValues( 8 );


        plt.figure();

        PressurePositionValues = example.getPressurePositionValues();

        PressureValuesConstTime10 = example.getPressureValuesConstTime( 10 );        
        PressureValuesConstTime20 = example.getPressureValuesConstTime( 20 );        
        PressureValuesConstTime40 = example.getPressureValuesConstTime( 40 );        
        PressureValuesConstTime100 = example.getPressureValuesConstTime( 100 );        
        PressureValuesConstTime200 = example.getPressureValuesConstTime( 200 );

        plt.plot( PressureValuesConstTime10, PressurePositionValues, 'bo', markersize = 4, label = '50' );
        plt.plot( PressureValuesConstTime20, PressurePositionValues, 'go', markersize = 4, label = '100' );
        plt.plot( PressureValuesConstTime40, PressurePositionValues, 'ro', markersize = 4, label = '200' );
        plt.plot( PressureValuesConstTime100, PressurePositionValues, 'co', markersize = 4, label = '500' );
        plt.plot( PressureValuesConstTime200, PressurePositionValues, 'mo', markersize = 4, label = '1000' );

        plt.xlabel( 'Pressure [Pa]' );
        plt.ylabel( 'Position [m]' );
        plt.legend( title = 'Time [s]', fontsize = 12, bbox_to_anchor = ( 1.14, 1.0 ) ); # bbox_to_anchor is used to push the legend around


##        plt.figure();
##
##        PressurePositionValuesNormalized = example.getPressurePositionValuesNormalized();
##
##        PressureValuesNormalizedConstTime20 = example.getPressureValuesNormalizedConstTime( 20 );        
##        PressureValuesNormalizedConstTime40 = example.getPressureValuesNormalizedConstTime( 40 );        
##        PressureValuesNormalizedConstTime100 = example.getPressureValuesNormalizedConstTime( 100 );        
##        PressureValuesNormalizedConstTime200 = example.getPressureValuesNormalizedConstTime( 200 );        
##        PressureValuesNormalizedConstTime400 = example.getPressureValuesNormalizedConstTime( 400 );
##
##        plt.plot( PressureValuesNormalizedConstTime20, PressurePositionValuesNormalized, 'bo', markersize = 4, label = '100' );
##        plt.plot( PressureValuesNormalizedConstTime40, PressurePositionValuesNormalized, 'go', markersize = 4, label = '200' );
##        plt.plot( PressureValuesNormalizedConstTime100, PressurePositionValuesNormalized, 'ro', markersize = 4, label = '500' );
##        plt.plot( PressureValuesNormalizedConstTime200, PressurePositionValuesNormalized, 'co', markersize = 4, label = '1000' );
##        plt.plot( PressureValuesNormalizedConstTime400, PressurePositionValuesNormalized, 'mo', markersize = 4, label = '2000' );
##
##        plt.xlabel( 'p / p_0' );
##        plt.ylabel( 'y / H' );
##        plt.legend( title = 'Time [s]', fontsize = 12, bbox_to_anchor = ( 1.14, 1.0 ) ); # bbox_to_anchor is used to push the legend around


##        plt.figure();
##
##        DisplacementPositionValues = example.getDisplacementPositionValues();
##
##        DisplacementValuesConstTime0 = example.getDisplacementValuesConstTime( 0 );
##        DisplacementValuesConstTime100 = example.getDisplacementValuesConstTime( 100 );
##        DisplacementValuesConstTime200 = example.getDisplacementValuesConstTime( 200 );
##        DisplacementValuesConstTime400 = example.getDisplacementValuesConstTime( 400 );
##
##        plt.plot( DisplacementValuesConstTime0, DisplacementPositionValues, 'bo', markersize = 4, label = '0' );
##        plt.plot( DisplacementValuesConstTime100, DisplacementPositionValues, 'go', markersize = 4, label = '500' );
##        plt.plot( DisplacementValuesConstTime200, DisplacementPositionValues, 'ro', markersize = 4, label = '1000' );
##        plt.plot( DisplacementValuesConstTime400, DisplacementPositionValues, 'co', markersize = 4, label = '2000' );
##
##        plt.xlabel( 'Displacement [m]' );
##        plt.ylabel( 'Position [m]' );
##        plt.legend( title = 'Time [s]', fontsize = 12 );


##        plt.figure();
##
##        PressureValuesConstPosition = example.getPressureValuesConstPositionDomainBase( 8 );
##
##        plt.plot( timeValues, PressureValuesConstPosition, 'ro', markersize = 3, label = 'Numerical Solution' );
##
##        plt.title( 'Domain Base' );
##        plt.xlabel( 'Time [s]' );
##        plt.ylabel( 'Pressure [Pa]' );
##        plt.legend( fontsize = 12 );


##        plt.figure();
##
##        PressureValuesNormalizedConstPosition = example.getPressureValuesNormalizedConstPositionDomainBase( 8 );
##
##        plt.plot( timeValues, PressureValuesNormalizedConstPosition, 'ro', markersize = 3, label = 'Numerical Solution' );
##
##        plt.title( 'Domain Base' );
##        plt.xlabel( 'Time [s]' );
##        plt.ylabel( 'p/p_0' );
##        plt.legend( fontsize = 12 );


##        plt.figure();
##
##        DisplacementValuesConstPosition = example.getDisplacementValuesConstPositionDomainTop( 8 );
##
##        plt.plot( timeValues, DisplacementValuesConstPosition, 'ro', markersize = 3, label = 'Numerical Solution' );
##
##        plt.title( 'Domain Top' );
##        plt.xlabel( 'Time [s]' );
##        plt.ylabel( 'Displacement [m]' );
##        plt.legend( fontsize = 12 );
        

        plt.show();
