# ABREVIATIONS USED =============================================================================


from NumericalGeomechanicsProblem import NumericalGeomechanicsProblem;


# CLASS DEFINITION ==============================================================================

class MandelsProblemNumerical( object ):

        # Constructor ---------------------------------------------------------------------------
        def __init__( self, NumericalGeomechanicsProblem ):
                self.numericalSolution = NumericalGeomechanicsProblem;
                

        # Internal functions --------------------------------------------------------------------


        # Class interface -----------------------------------------------------------------------
        def getPressureXPositionValues( self ):
                positionValues = [ ];
                
                for i in range( 0, ( ( self.numericalSolution.getNodesXPositionsArray() ).shape )[ 1 ] ):
                        positionValues.append( ( self.numericalSolution.getNodesXPositionsArray() )[ 0, i ] );
                        
                return positionValues;

        
        def getPressureXPositionValuesNormalized( self ):
                positionValues = self.getPressureXPositionValues();
                positionValuesNormalized = [ ];
                lenght = self.numericalSolution.getLenght();

                for i in range( 0, len( positionValues ) ):
                        positionValuesNormalized.append( positionValues[ i ] / lenght );

                return positionValuesNormalized;


        def getHorDisplacementXPositionValues( self ):
                positionValues = [ ];

                for i in range( 0, ( ( self.numericalSolution.getHorDisplacementXPositionsArray() ).shape )[ 1 ] ):
                        positionValues.append( ( self.numericalSolution.getHorDisplacementXPositionsArray() )[ 0, i ] );

                return positionValues;


        def getHorDisplacementXPositionValuesNormalized( self ):
                positionValues = self.getHorDisplacementXPositionValues();
                positionValuesNormalized = [ ];
                lenght = self.numericalSolution.getLenght();

                for i in range( 0, len( positionValues ) ):
                        positionValuesNormalized.append( positionValues[ i ] / lenght );

                return positionValuesNormalized;


        def getVertDisplacementYPositionValues( self ):
                positionValues = [ ];

                for i in range( 0, ( ( self.numericalSolution.getVertDisplacementYPositionsArray() ).shape )[ 0 ] ):
                        positionValues.append( ( self.numericalSolution.getVertDisplacementYPositionsArray() )[ i, 0 ] );

                return positionValues;


        def getVertDisplacementYPositionValuesNormalized( self ):
                positionValues = self.getVertDisplacementYPositionValues();
                positionValuesNormalized = [ ];
                height = self.numericalSolution.getHeight();

                for i in range( 0, len( positionValues ) ):
                        positionValuesNormalized.append( positionValues[ i ] / height );

                return positionValuesNormalized;
        

        def getPressureValuesConstTime( self, timeStep ):
                pressureValues = [ ];

                for i in range( 0, ( ( self.numericalSolution.getPressureFieldArray()[ timeStep ] ).shape )[ 1 ] ):
                        pressureValues.append( ( self.numericalSolution.getPressureFieldArray()[ timeStep ] )[ 0, i ] );

                return pressureValues;


        def getPressureValuesNormalizedConstTime( self, timeStep ):
                pressureValues = self.getPressureValuesConstTime( timeStep );
                pressureValuesNormalized = [ ];
                initialPressure = ( self.numericalSolution.getPressureFieldArray()[ 0 ] )[ 0, 0 ];

                for i in range( 0, len( pressureValues ) ):
                        pressureValuesNormalized.append( pressureValues[ i ] / initialPressure );

                return pressureValuesNormalized;


        def getHorDisplacementValuesConstTime( self, timeStep ):
                displacementValues = [ ];

                for i in range( 0, ( ( self.numericalSolution.getHorDisplacementFieldArray()[ timeStep ] ).shape )[ 1 ] ):
                        displacementValues.append( ( self.numericalSolution.getHorDisplacementFieldArray()[ timeStep ] )[ 0, i ] );

                return displacementValues;


        def getVertDisplacementValuesConstTime( self, timeStep ):
                displacementValues = [ ];

                for i in range( 0, ( ( self.numericalSolution.getVertDisplacementFieldArray()[ timeStep ] ).shape )[ 0 ] ):
                        displacementValues.append( ( self.numericalSolution.getVertDisplacementFieldArray()[ timeStep ] )[ i, 0 ] );

                return displacementValues;


        def getVertStressValuesConstTime( self, timeStep ):
                stressValues = [ ];

                for i in range( 0, ( ( self.numericalSolution.getVertStressFieldArray()[ timeStep ] ).shape )[ 1 ] ):
                        stressValues.append( ( self.numericalSolution.getVertStressFieldArray()[ timeStep ] )[ 0, i ] );

                return stressValues;


        def getTimeValues( self, timeStepsBetweenPoints ):
                timeValues = [ ];

                i = 0;
                while i < len( self.numericalSolution.getTimeSteps() ):
                        timeValues.append( ( self.numericalSolution.getTimeSteps() )[ i ] );
                        i += timeStepsBetweenPoints;

                return timeValues;


        def getPressureValuesConstPositionSouthWestVolume( self, timeStepsBetweenPoints ):
                pressureValues = [ ];

                i = 0;
                while i < len( self.numericalSolution.getPressureFieldArray() ):
                        pressureValues.append( ( self.numericalSolution.getPressureFieldArray()[ i ] )[ 0, 0 ] );
                        i += timeStepsBetweenPoints;

                return pressureValues;


        def getPressureValuesNormalizedConstPositionSouthWestVolume( self, timeStepsBetweenPoints ):
                pressureValues = self.getPressureValuesConstPositionSouthWestVolume( timeStepsBetweenPoints );
                pressureValuesNormalized = [ ];
                initialPressure = ( self.numericalSolution.getPressureFieldArray()[ 0 ] )[ 0, 0 ];

                for i in range( 0, len( pressureValues ) ):
                        pressureValuesNormalized.append( pressureValues[ i ] / initialPressure );

                return pressureValuesNormalized;


        def getHorDisplacementValuesConstPositionEastBoundary( self, timeStepsBetweenPoints ):
                displacementValues = [ ];

                i = 0;
                while i < len( self.numericalSolution.getHorDisplacementFieldArray() ):
                        displacementValues.append( ( self.numericalSolution.getHorDisplacementFieldArray()[ i ] )
                                                   [ 0, ( self.numericalSolution.getHorDisplacementFieldArray()[ i ] ).shape[ 1 ] - 1 ] );
                        i += timeStepsBetweenPoints;

                return displacementValues;


        def getVertDisplacementValuesConstPositionNorthBoundary( self, timeStepsBetweenPoints ):
                displacementValues = [ ];

                i = 0;
                while i < len( self.numericalSolution.getVertDisplacementFieldArray() ):
                        displacementValues.append( ( self.numericalSolution.getVertDisplacementFieldArray()[ i ] )
                                                   [ ( self.numericalSolution.getVertDisplacementFieldArray()[ i ] ).shape[ 0 ] - 1, 0 ] );
                        i += timeStepsBetweenPoints;

                return displacementValues;


        def getVertStressValuesConstPositionSouthWestVolume( self, timeStepsBetweenPoints ):
                stressValues = [ ];

                i = 0;
                while i < len( self.numericalSolution.getVertStressFieldArray() ):
                        stressValues.append( ( self.numericalSolution.getVertStressFieldArray()[ i ] )[ 0, 0 ] );
                        i += timeStepsBetweenPoints;

                return stressValues;
                
        
        def getVertStressValuesConstPositionSouthEastVolume( self, timeStepsBetweenPoints ):
                stressValues = [ ];

                i = 0;
                while i < len( self.numericalSolution.getVertStressFieldArray() ):
                        stressValues.append( ( self.numericalSolution.getVertStressFieldArray()[ i ] )
                                                   [ 0, ( self.numericalSolution.getVertStressFieldArray()[ i ] ).shape[ 1 ] - 1 ] );
                        i += timeStepsBetweenPoints;

                return stressValues;

        
# EXAMPLE =======================================================================================

import matplotlib.pyplot as plt

if __name__ == '__main__':
        results = NumericalGeomechanicsProblem( "D:\\PostProcessing\\Numerical\\Python\\MandelsProblemNumerical\\PythonTransientResults.dat" );
        example = MandelsProblemNumerical( results );

        PressureXPositionValues = example.getPressureXPositionValues();
        PressureXPositionValuesNormalized = example.getPressureXPositionValuesNormalized();
        HorDisplacementXPositionValues = example.getHorDisplacementXPositionValues();
        VertDisplacementYPositionValues = example.getVertDisplacementYPositionValues();

        timeStepsBetweenPoints = 6;
        TimeValues = example.getTimeValues( timeStepsBetweenPoints );

        
##        plt.figure();
##
##        PressureValuesConstTime0 = example.getPressureValuesConstTime( 0 );
##        PressureValuesConstTime100 = example.getPressureValuesConstTime( 4 );
##        PressureValuesConstTime1000 = example.getPressureValuesConstTime( 40 );
##        PressureValuesConstTime3000 = example.getPressureValuesConstTime( 120 );
##
##        plt.plot( PressureXPositionValues, PressureValuesConstTime0, 'bo', markersize = 4, label = '0' );
##        plt.plot( PressureXPositionValues, PressureValuesConstTime100, 'go', markersize = 4, label = '100' );
##        plt.plot( PressureXPositionValues, PressureValuesConstTime1000, 'ro', markersize = 4, label = '1000' );
##        plt.plot( PressureXPositionValues, PressureValuesConstTime3000, 'co', markersize = 4, label = '3000' );
##
##        plt.grid( True );
##        plt.xlabel( 'Position [m]' );
##        plt.ylabel( 'Pressure [Pa]' );
##        plt.legend( title = 'Time [s]', fontsize = 12, loc = 'best' );


##        plt.figure();
##
##        PressureValuesNormalizedConstTime0 = example.getPressureValuesNormalizedConstTime( 0 );
##        PressureValuesNormalizedConstTime100 = example.getPressureValuesNormalizedConstTime( 4 );
##        PressureValuesNormalizedConstTime1000 = example.getPressureValuesNormalizedConstTime( 40 );
##        PressureValuesNormalizedConstTime3000 = example.getPressureValuesNormalizedConstTime( 120 );
##
##        plt.plot( PressureXPositionValuesNormalized, PressureValuesNormalizedConstTime0, 'bo', markersize = 4, label = '0' );
##        plt.plot( PressureXPositionValuesNormalized, PressureValuesNormalizedConstTime100, 'go', markersize = 4, label = '100' );
##        plt.plot( PressureXPositionValuesNormalized, PressureValuesNormalizedConstTime1000, 'ro', markersize = 4, label = '1000' );
##        plt.plot( PressureXPositionValuesNormalized, PressureValuesNormalizedConstTime3000, 'co', markersize = 4, label = '3000' );
##
##        plt.grid( True );
##        plt.xlabel( 'x/L' );
##        plt.ylabel( 'p/p_0' );
##        plt.legend( title = 'Time [s]', fontsize = 12, loc = 'best' );


##        plt.figure();
##
##        HorDisplacementValuesConstTime0 = example.getHorDisplacementValuesConstTime( 0 );
##        HorDisplacementValuesConstTime1000 = example.getHorDisplacementValuesConstTime( 40 );
##        HorDisplacementValuesConstTime3000 = example.getHorDisplacementValuesConstTime( 120 );
##
##        plt.plot( HorDisplacementXPositionValues, HorDisplacementValuesConstTime0, 'bo', markersize = 4, label = '0' );
##        plt.plot( HorDisplacementXPositionValues, HorDisplacementValuesConstTime1000, 'ro', markersize = 4, label = '1000' );
##        plt.plot( HorDisplacementXPositionValues, HorDisplacementValuesConstTime3000, 'co', markersize = 4, label = '3000' );
##
##        plt.grid( True );
##        plt.xlabel( 'Position [m]' );
##        plt.ylabel( 'Horizontal displacement (u) [m]' );
##        plt.legend( title = 'Time [s]', fontsize = 12, loc = 'best' );


##        plt.figure();
##
##        VertDisplacementValuesConstTime0 = example.getVertDisplacementValuesConstTime( 0 );
##        VertDisplacementValuesConstTime1000 = example.getVertDisplacementValuesConstTime( 40 );
##        VertDisplacementValuesConstTime3000 = example.getVertDisplacementValuesConstTime( 120 );
##
##        plt.plot( VertDisplacementValuesConstTime0, VertDisplacementYPositionValues, 'bo', markersize = 4, label = '0' );
##        plt.plot( VertDisplacementValuesConstTime1000, VertDisplacementYPositionValues, 'ro', markersize = 4, label = '1000' );
##        plt.plot( VertDisplacementValuesConstTime3000, VertDisplacementYPositionValues, 'co', markersize = 4, label = '3000' );
##
##        plt.grid( True );
##        plt.xlabel( 'Vertical displacement (v) [m]' );
##        plt.ylabel( 'Position [m]' );
##        plt.legend( title = 'Time [s]', fontsize = 12, loc = 'best' );


##        plt.figure();
##
##        VertStressValuesConstTime0 = example.getVertStressValuesConstTime( 0 );
##        VertStressValuesConstTime100 = example.getVertStressValuesConstTime( 4 );
##        VertStressValuesConstTime1000 = example.getVertStressValuesConstTime( 40 );
##        VertStressValuesConstTime3000 = example.getVertStressValuesConstTime( 120 );
##
##        plt.plot( PressureXPositionValues, VertStressValuesConstTime0, 'bo', markersize = 4, label = '0' );
##        plt.plot( PressureXPositionValues, VertStressValuesConstTime100, 'go', markersize = 4, label = '100' );
##        plt.plot( PressureXPositionValues, VertStressValuesConstTime1000, 'ro', markersize = 4, label = '1000' );
##        plt.plot( PressureXPositionValues, VertStressValuesConstTime3000, 'co', markersize = 4, label = '3000' );
##
##        plt.grid( True );
##        plt.xlabel( 'Position [m]' );
##        plt.ylabel( 'Vertical normal stress [Pa]' );
##        plt.legend( title = 'Time [s]', fontsize = 12, loc = 'best' );


##        plt.figure();
##
##        PressureValuesConstPosition = example.getPressureValuesConstPositionSouthWestVolume( timeStepsBetweenPoints );
##
##        plt.plot( TimeValues, PressureValuesConstPosition, 'ro', markersize = 4, label = 'Numerical Solution' );
##
##        plt.grid( True );
##        plt.xlabel( 'Time [s]' );
##        plt.ylabel( 'Pressure [Pa]' );
##        plt.legend( fontsize = 12, loc = 'best' );


##        plt.figure();
##
##        PressureValuesNormalizedConstPosition = example.getPressureValuesNormalizedConstPositionSouthWestVolume( timeStepsBetweenPoints );
##
##        plt.plot( TimeValues, PressureValuesNormalizedConstPosition, 'ro', markersize = 4, label = 'Numerical Solution' );
##
##        plt.grid( True );
##        plt.xlabel( 'Time [s]' );
##        plt.ylabel( 'p/p_0' );
##        plt.legend( fontsize = 12, loc = 'best' );
        

##        plt.figure();
##
##        HorDisplacementValuesConstPosition = example.getHorDisplacementValuesConstPositionEastBoundary( timeStepsBetweenPoints );
##
##        plt.plot( TimeValues, HorDisplacementValuesConstPosition, 'ro', markersize = 4, label = 'Numerical Solution' );
##
##        plt.grid( True );
##        plt.xlabel( 'Time [s]' );
##        plt.ylabel( 'Horizontal displacement (u) [m]' );
##        plt.legend( fontsize = 12, loc = 'best', fancybox = True, shadow = True, numpoints = 1 );


##        plt.figure();
##
##        VertDisplacementValuesConstPosition = example.getVertDisplacementValuesConstPositionNorthBoundary( timeStepsBetweenPoints );
##
##        plt.plot( TimeValues, VertDisplacementValuesConstPosition, 'ro', markersize = 4, label = 'Numerical Solution' );
##
##        plt.grid( True );
##        plt.xlabel( 'Time [s]' );
##        plt.ylabel( 'Vertical displacement (v) [m]' );
##        plt.legend( fontsize = 12, loc = 'best', fancybox = True, shadow = True, numpoints = 1 );


##        plt.figure();
##
##        VertStressValuesConstPositionWest = example.getVertStressValuesConstPositionSouthWestVolume( timeStepsBetweenPoints );
##        VertStressValuesConstPositionEast = example.getVertStressValuesConstPositionSouthEastVolume( timeStepsBetweenPoints );
##
##        plt.plot( TimeValues, VertStressValuesConstPositionWest, 'ro', markersize = 4, label = 'West' );
##        plt.plot( TimeValues, VertStressValuesConstPositionEast, 'bo', markersize = 4, label = 'East' );
##
##        plt.grid( True );
##        plt.xlabel( 'Time [s]' );
##        plt.ylabel( 'Vertical normal stress [Pa]' );
##        plt.legend( fontsize = 12, loc = 'best', fancybox = True, shadow = True, numpoints = 1, title = 'Position' );


        plt.show();
